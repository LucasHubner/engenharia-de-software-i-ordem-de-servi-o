package br.com.unioeste.esi.teste.veiculo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.manager.veiculo.ManagerMarca;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerMarca {
        
    public static void testCreate(){
        ManagerMarca manager = new ManagerMarca();

        Marca marca = new Marca();
        marca.setNome("Ford");
        try {
            marca = manager.CadastraMarca(marca);
            System.out.println(marca.getId());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerMarca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerMarca manager = new ManagerMarca();

        try {
            ArrayList<Marca> marcas = manager.listaMarcas();
            for(Marca marca:marcas){
                System.out.println(marca.getNome());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerMarca.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerMarca manager = new ManagerMarca();
        int idMarca = 1;
        try{
            Marca marca = manager.recuperaMarca(idMarca);
            if (marca == null) {
                System.out.println("null");
            }
            else{
                System.out.println(marca.getNome());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerMarca manager = new ManagerMarca();
        Marca marca = new Marca();
        marca.setId(1);
        try{
            manager.excluiMarca(marca);
            if (marca.getNome() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
