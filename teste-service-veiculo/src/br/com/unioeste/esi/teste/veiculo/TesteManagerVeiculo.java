package br.com.unioeste.esi.teste.veiculo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.bo.veiculo.Veiculo;
import br.com.unioeste.esi.manager.veiculo.ManagerVeiculo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerVeiculo {
        
    public static void testCreate(){
        ManagerVeiculo manager = new ManagerVeiculo();

        Veiculo veiculo = new Veiculo();
        veiculo.setAno(2014);
        veiculo.setChassi("198273123789");
        
        Cor cor = new Cor();
        cor.setId(1);
        veiculo.setCor(cor);
        
        Modelo modelo = new Modelo();
        modelo.setId(4);
        veiculo.setModelo(modelo);
        
        veiculo.setPlaca("ABC - 0001");
        try {
            veiculo = manager.CadastraVeiculo(veiculo);
            System.out.println(veiculo.getPlaca());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerVeiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerVeiculo manager = new ManagerVeiculo();

        try {
            ArrayList<Veiculo> veiculos = manager.listaVeiculos();
            for(Veiculo veiculo:veiculos){
                System.out.print(veiculo.getPlaca());
                System.out.print(" - ");
                System.out.print(veiculo.getModelo().getNome());
                System.out.print(" - ");
                System.out.println(veiculo.getModelo().getMarca().getNome());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerVeiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerVeiculo manager = new ManagerVeiculo();
        String placaVeiculo = "ABC - 0001";
        try{
            Veiculo veiculo = manager.recuperaVeiculo(placaVeiculo);
            if (veiculo == null) {
                System.out.println("null");
            }
            else{
                System.out.println(veiculo.getPlaca());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerVeiculo manager = new ManagerVeiculo();
        Veiculo veiculo = new Veiculo();
        veiculo.setPlaca("ABC - 0001");
        try{
            manager.excluiVeiculo(veiculo);
            if (veiculo.getPlaca() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException{
//        testCreate();
//        testList();
//        testRecupera();
        testExclui();
    }

}
