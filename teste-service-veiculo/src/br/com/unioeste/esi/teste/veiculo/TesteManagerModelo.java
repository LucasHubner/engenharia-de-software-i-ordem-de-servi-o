package br.com.unioeste.esi.teste.veiculo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.manager.veiculo.ManagerModelo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerModelo {
        
    public static void testCreate(){
        ManagerModelo manager = new ManagerModelo();

        Modelo modelo = new Modelo();
        Marca marca = new Marca();
        marca.setId(2);
        
        modelo.setNome("Fiesta");
        modelo.setMarca(marca);
        try {
            modelo = manager.CadastraModelo(modelo);
            System.out.println(modelo.getId());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerModelo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerModelo manager = new ManagerModelo();

        try {
            ArrayList<Modelo> modelos = manager.listaModelos(1);
            for(Modelo modelo:modelos){
                System.out.print(modelo.getMarca().getNome());
                System.out.print(" - ");
                System.out.println(modelo.getNome());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerModelo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerModelo manager = new ManagerModelo();
        int idModelo = 3;
        try{
            Modelo modelo = manager.recuperaModelo(idModelo);
            if (modelo == null) {
                System.out.println("null");
            }
            else{
                System.out.println(modelo.getNome());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerModelo manager = new ManagerModelo();
        Modelo modelo = new Modelo();
        modelo.setId(3);
        try{
            manager.excluiModelo(modelo);
            if (modelo.getNome() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
