package br.com.unioeste.esi.teste.veiculo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.manager.veiculo.ManagerCor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerCor {
        
    public static void testCreate(){
        ManagerCor manager = new ManagerCor();

        Cor cor = new Cor();
        cor.setNome("Vermelho");
        try {
            cor = manager.CadastraCor(cor);
            System.out.println(cor.getId());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerCor manager = new ManagerCor();

        try {
            ArrayList<Cor> cors = manager.listaCores();
            for(Cor cor:cors){
                System.out.println(cor.getNome());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerCor manager = new ManagerCor();
        int idCor = 2;
        try{
            Cor cor = manager.recuperaCor(idCor);
            if (cor == null) {
                System.out.println("null");
            }
            else{
                System.out.println(cor.getNome());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerCor manager = new ManagerCor();
        Cor cor = new Cor();
        cor.setId(2);
        try{
            manager.excluiCor(cor);
            if (cor.getNome() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException{
//        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
