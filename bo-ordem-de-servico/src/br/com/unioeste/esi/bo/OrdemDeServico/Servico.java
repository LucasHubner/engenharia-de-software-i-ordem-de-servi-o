/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.OrdemDeServico;

import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class Servico implements Serializable {
    int nroOrdemDeServico;
    float tempo;
    float valor;
    TipoServico tipoServico;

    public int getNroOrdemDeServico() {
        return nroOrdemDeServico;
    }

    public void setNroOrdemDeServico(int nroOrdemDeServico) {
        this.nroOrdemDeServico = nroOrdemDeServico;
    }

    public float getTempo() {
        return tempo;
    }

    public void setTempo(float tempo) {
        this.tempo = tempo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public TipoServico getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
    }
    
}
