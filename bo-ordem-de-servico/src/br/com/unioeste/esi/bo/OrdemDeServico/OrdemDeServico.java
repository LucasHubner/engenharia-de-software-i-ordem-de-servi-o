/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.OrdemDeServico;

import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.bo.veiculo.Veiculo;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author lucas
 */
public class OrdemDeServico implements Serializable{
    int numero;
    Date dataAbertura;
    EstadoOS estado;
    String descricao;
    Veiculo veiculo;
    Cliente cliente;
    Date dataFechamento;

    public OrdemDeServico() {
        this.dataFechamento = new Date();
        this.dataAbertura = new Date();
        this.estado = new EstadoOS();
        this.descricao = new String();
        this.veiculo = new Veiculo();
        this.cliente = new Cliente();
    }

    public Date getDataFechamento() {
        return dataFechamento;
    }

    public void setDataFechamento(Date dataFechamento) {
        this.dataFechamento = dataFechamento;
    }
    
    

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public EstadoOS getEstado() {
        return estado;
    }

    public void setEstado(EstadoOS estado) {
        this.estado = estado;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
    
}
