/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class Cep implements Serializable{
    String cepString;
    int cepInt;

    public Cep() {
    }

    public Cep(String cepString, int cepInt) {
        this.cepString = cepString;
        this.cepInt = cepInt;
    }

    public String getCepString() {
        return cepString;
    }

    public void setCepString(String cepString) {
        this.cepString = cepString;
    }

    public int getCepInt() {
        return cepInt;
    }

    public void setCepInt(int cepInt) {
        this.cepInt = cepInt;
    }

    public boolean isEmpty(){
        if (this.cepString == null || this.cepString.equals("")){
            return true;
        }
        return false;
    }
    
}
