/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;
import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class Logradouro implements Serializable {
    int idLogradouro;
    String nomeLogradouro;
    TipoLogradouro tipoLogradouro;

    public Logradouro() {
    }

    public Logradouro(int idLogradouro, String nomeLogradouro, TipoLogradouro tipoLogradouro) {
        this.idLogradouro = idLogradouro;
        this.nomeLogradouro = nomeLogradouro;
        this.tipoLogradouro = tipoLogradouro;
    }

    public Logradouro(String nomeLogradouro, TipoLogradouro tipoLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
        this.tipoLogradouro = tipoLogradouro;
    }
    

    public int getIdLogradouro() {
        return idLogradouro;
    }

    public void setIdLogradouro(int idLogradouro) {
        this.idLogradouro = idLogradouro;
    }

    public String getNomeLogradouro() {
        return nomeLogradouro;
    }

    public void setNomeLogradouro(String nomeLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
    }

    public TipoLogradouro getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }
    
    public boolean isEmpty(){
        if (this.nomeLogradouro == null || this.nomeLogradouro.equals("")){
            return true;
        }
        return false;
    }
    
}
