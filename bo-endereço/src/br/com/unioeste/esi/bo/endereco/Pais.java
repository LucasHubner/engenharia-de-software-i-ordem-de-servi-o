/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class Pais implements Serializable {
    int idPais;
    String nomePais;
    String siglaPais;

    public Pais() {
    }

    public Pais(String nomePais, String siglaPais) {
        this.nomePais = nomePais;
        this.siglaPais = siglaPais;
    }

    public Pais(int idPais, String nomePais, String siglaPais) {
        this.idPais = idPais;
        this.nomePais = nomePais;
        this.siglaPais = siglaPais;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getNomePais() {
        return nomePais;
    }

    public void setNomePais(String nomePais) {
        this.nomePais = nomePais;
    }

    public String getSiglaPais() {
        return siglaPais;
    }

    public void setSiglaPais(String siglaPais) {
        this.siglaPais = siglaPais;
    }
    
    public boolean isEmpty(){
        if (this.nomePais == null || this.nomePais.equals("")){
            return true;
        }
        return false;
    }
}
