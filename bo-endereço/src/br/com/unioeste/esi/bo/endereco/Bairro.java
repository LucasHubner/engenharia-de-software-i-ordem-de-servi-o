/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class Bairro implements Serializable{
    int idBairro;
    String nomeBairro = new String();

    public Bairro() {
    }

    public Bairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public Bairro(int idBairro, String nomeBairro) {
        this.idBairro = idBairro;
        this.nomeBairro = nomeBairro;
    }

    public int getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(int idBairro) {
        this.idBairro = idBairro;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }
    
    public boolean isEmpty(){
        if (this.nomeBairro == null || this.nomeBairro.equals("")){
            return true;
        }
        return false;
    }
    
}
