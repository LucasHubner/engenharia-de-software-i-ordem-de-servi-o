/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;

import br.com.unioeste.esi.bo.endereco.Endereco;
import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class EnderecoEspecifico implements Serializable {
    int nroEndereco;
    String complementoEndereco = new String();
    Endereco endereco;

    public EnderecoEspecifico() {
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    public int getNroEndereco() {
        return nroEndereco;
    }

    public void setNroEndereco(int nroEndereco) {
        this.nroEndereco = nroEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }
    
    
    
}
