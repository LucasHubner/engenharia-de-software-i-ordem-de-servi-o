/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.endereco;
import java.io.Serializable;
/**
 *
 * @author lucas
 */
public class Cidade implements Serializable{
    int idCidade;
    String nomeCidade;
    UnidadeFederativa unidadeFederativa;

    public Cidade() {
    }

    public Cidade(String nomeCidade, UnidadeFederativa unidadeFederativa) {
        this.nomeCidade = nomeCidade;
        this.unidadeFederativa = unidadeFederativa;
    }

    public Cidade(int idCidade, String nomeCidade, UnidadeFederativa unidadeFederativa) {
        this.idCidade = idCidade;
        this.nomeCidade = nomeCidade;
        this.unidadeFederativa = unidadeFederativa;
    }

    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public UnidadeFederativa getUnidadeFederativa() {
        return unidadeFederativa;
    }

    public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
        this.unidadeFederativa = unidadeFederativa;
    }
    
    public boolean isEmpty(){
        if (this.nomeCidade == null || this.nomeCidade.equals("")){
            return true;
        }
        return false;
    }
}
