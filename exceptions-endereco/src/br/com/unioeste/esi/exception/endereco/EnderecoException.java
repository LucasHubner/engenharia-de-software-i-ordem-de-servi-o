/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.exception.endereco;

/**
 *
 * @author lucas
 */
public class EnderecoException extends Exception{

    private String msg;
    
    public EnderecoException(String msg){
        super(msg);
    }
    
    public String getMsg(){
        return msg;
    }
}
