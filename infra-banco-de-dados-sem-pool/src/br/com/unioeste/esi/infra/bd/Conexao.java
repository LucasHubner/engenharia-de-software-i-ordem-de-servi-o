/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.infra.bd;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lucas
 */
public class Conexao implements Serializable {
    private Connection conexao;
    private PreparedStatement declaracao;
    private String database = "OrdemDeServico";
    
    public boolean AbreConexao(){
        try{
            Class.forName("com.mysql.jdbc.Driver");  
            String url = "jdbc:mysql://localhost:3306/" + database;
            String usuario = "root";
            String senha = "";
            conexao = DriverManager.getConnection(url,usuario,senha);
            return true;
        }
        catch(ClassNotFoundException | SQLException e){
            System.out.print(e);
            return false;
        }
    }
    
    public boolean FechaConexao(){
        try{
            conexao.close();
            return true;
        }
        catch(SQLException e){
            System.out.print(e);
            return false;
        }
    }

    public Connection getConexao() {
        return conexao;
    }

    public void setConexao(Connection conexao) {
        this.conexao = conexao;
    }

    public PreparedStatement getDeclaracao() {
        return declaracao;
    }

    public void setDeclaracao(String declaracao) throws SQLException {
        this.declaracao = this.conexao.prepareStatement(declaracao, Statement.RETURN_GENERATED_KEYS);
        
    }
    public int getLastInsertedId() throws SQLException{
        ResultSet resultado = declaracao.getGeneratedKeys();
        resultado.next();
        return resultado.getInt(1);
    }
}
