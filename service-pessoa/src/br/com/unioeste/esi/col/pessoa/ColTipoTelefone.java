/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import br.com.unioeste.esi.bo.pessoa.TipoTelefone;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColTipoTelefone {
    Conexao conexao;

    public ColTipoTelefone(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private TipoTelefone PopulaTipoTelefone(ResultSet resultado) throws SQLException{
        TipoTelefone tipo = new TipoTelefone();
        tipo.setId(resultado.getInt("idTipoTelefone"));
        tipo.setNome(resultado.getString("nomeTipoTelefone"));
        
        return tipo;
    }
    
    public TipoTelefone Create(TipoTelefone tipo) throws SQLException{
        conexao.setDeclaracao("INSERT INTO TipoTelefone VALUES (null, ?) ");
        conexao.getDeclaracao().setString(1,tipo.getNome());
        conexao.getDeclaracao().executeUpdate();
        tipo.setId(conexao.getLastInsertedId());
        
        return tipo;
    }

    public TipoTelefone Read(TipoTelefone tipo) throws SQLException{
        conexao.setDeclaracao("SELECT * FROM TipoTelefone WHERE idTipoTelefone = ?");
        conexao.getDeclaracao().setInt(1, tipo.getId());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        if (!resultado.isBeforeFirst()){
            return new TipoTelefone();
        }
        resultado.next();
        
        return PopulaTipoTelefone(resultado);
    }
    
    public ArrayList<TipoTelefone> List() throws SQLException{
        ArrayList<TipoTelefone> tipos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM TipoTelefone");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        while(resultado.next()){
            tipos.add(PopulaTipoTelefone(resultado));
        }
        return tipos;
    }
    
}
