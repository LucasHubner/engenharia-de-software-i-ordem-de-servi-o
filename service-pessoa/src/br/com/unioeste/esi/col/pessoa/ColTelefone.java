/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import br.com.unioeste.esi.bo.pessoa.DDD;
import br.com.unioeste.esi.bo.pessoa.Telefone;
import br.com.unioeste.esi.bo.pessoa.TipoTelefone;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColTelefone {
    
    Conexao conexao;

    public ColTelefone(Conexao conexao) {
        this.conexao = conexao;
    }
    
    
    private Telefone PopulaTelefone(ResultSet resultado) throws SQLException{
        Telefone telefone = new Telefone();
        telefone.setIdCliente(resultado.getInt("idCliente"));
        DDD ddd = new DDD();
        ddd.setArea(resultado.getInt("codArea"));
        ddd.setPais(resultado.getInt("codPais"));
        telefone.setDdd(ddd);
        telefone.setTelefone(resultado.getInt("nroTelefone"));
        ColTipoTelefone col = new ColTipoTelefone(conexao);
        TipoTelefone tipo = new TipoTelefone();
        tipo.setId(resultado.getInt("idTipoTelefone"));
        telefone.setTipoTelefone(col.Read(tipo));
        
        return telefone;
    }
    
    
    public Telefone Create(Telefone telefone) throws SQLException{
        conexao.setDeclaracao("INSERT INTO Telefone VALUES (?, ?, ?, ?, ?)");
        conexao.getDeclaracao().setInt(1, telefone.getIdCliente());
        conexao.getDeclaracao().setInt(2, telefone.getTipoTelefone().getId());
        conexao.getDeclaracao().setInt(3, telefone.getDdd().getArea());
        conexao.getDeclaracao().setInt(4, telefone.getDdd().getPais());
        conexao.getDeclaracao().setInt(5, telefone.getTelefone());

        conexao.getDeclaracao().executeUpdate();
        
        return telefone;
    }
    
    public Telefone Read(Telefone telefone) throws SQLException{
        conexao.setDeclaracao("SELECT * FROM Telefone WHERE idTelefone = ?");
        conexao.getDeclaracao().setInt(1, telefone.getIdCliente());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        return PopulaTelefone(resultado);
    }
    
    public ArrayList<Telefone> ListByClient(int idCliente) throws SQLException{
        ArrayList<Telefone> telefones = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Telefone WHERE idCliente = ?");
        conexao.getDeclaracao().setInt(1, idCliente);
        
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        while(resultado.next()){
            telefones.add(PopulaTelefone(resultado));
        }
        
        return telefones;
    }
}
