/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.bo.endereco.EnderecoEspecifico;
import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import br.com.unioeste.esi.bo.pessoa.juridica.CNPJ;
import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.bo.pessoa.fisica.CPF;
import br.com.unioeste.esi.bo.pessoa.fisica.Genero;
import br.com.unioeste.esi.bo.pessoa.NomePessoaFisica;
import br.com.unioeste.esi.bo.pessoa.NomePessoaJuridica;
import br.com.unioeste.esi.bo.pessoa.fisica.PessoaFisica;
import br.com.unioeste.esi.bo.pessoa.juridica.PessoaJuridica;
import br.com.unioeste.esi.bo.pessoa.fisica.RG;
import br.com.unioeste.esi.bo.pessoa.Telefone;
import br.com.unioeste.esi.col.endereco.ColEndereco;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColCliente {
  
    private Conexao conexao;

    public ColCliente(Conexao conexao) {
        this.conexao = conexao;
    }  
    private void SetEndereco(EnderecoEspecifico endereco) throws SQLException{
        conexao.getDeclaracao().setInt(7,endereco.getEndereco().getIdEndereco());
        conexao.getDeclaracao().setString(8,endereco.getComplementoEndereco());
        conexao.getDeclaracao().setInt(9,endereco.getNroEndereco());
        
    }
    
    private void SetTelefones(ArrayList<Telefone> telefones) throws SQLException{
        ColTelefone col = new ColTelefone(conexao);
        for(Telefone telefone:telefones){
            col.Create(telefone);
        }
    }
    
    private Cliente PopulaCliente(ResultSet resultado) throws SQLException{
        Cliente cliente = new Cliente();
        String cnpj = new String();
        cnpj = resultado.getString("cnpj");
        
        // ID
        cliente.setIdCliente(resultado.getInt("idCliente"));
        
        // Pega o endereço
        Endereco endereco = new Endereco();
        endereco.setIdEndereco(resultado.getInt("idEndereco"));
        ColEndereco colEndereco = new ColEndereco(conexao);
        endereco = colEndereco.Read(endereco.getIdEndereco());
        EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico();
        enderecoEspecifico.setEndereco(endereco);
        enderecoEspecifico.setNroEndereco(resultado.getInt("nroEndereco"));
        enderecoEspecifico.setComplementoEndereco(resultado.getString("complementoEndereco"));
        // Telefones
        ArrayList<Telefone> telefones = new ArrayList<>();
        telefones = new ColTelefone(conexao).ListByClient(cliente.getIdCliente());

        if(cnpj != null && !cnpj.isEmpty()){
            PessoaJuridica pessoaJuridica = new PessoaJuridica();
            pessoaJuridica.setEnderecoPrincipal(enderecoEspecifico);
            
            //CNPJ
            CNPJ cnpjClass = new CNPJ();
            cnpjClass.setNumeroEstendido(cnpj);
            cnpjClass.setDataEmissao(resultado.getDate("dataEmissaoCNPJ"));
            pessoaJuridica.setCnpj(cnpjClass);
            
            // Nome da Empresa
            // Primeiro arruma as heranças, pessoa fisica s tem nome fisico,
            // Juridico s tem nome juridico.
            NomePessoaJuridica nome = new NomePessoaJuridica();
            nome.setNomeEmpresa(resultado.getString("NomeEmpresa"));
            nome.setNomeFantasia(resultado.getString("NomeFantasia"));
            //Agora arruma as heranças.
            pessoaJuridica.setNomeJ(nome);
            /*
                Pessoa
                    Pessoa Fisica
                        Nome pessoa Fisica
                    Pessoa Juridica
                        Nome Pessoa Juridica
            */
            
            
            // Recupera atividades comerciais da empresa
            ColCliente_has_AtividadeComercial col = new ColCliente_has_AtividadeComercial(conexao);
            pessoaJuridica.setAtividadesComerciais(col.ListClientAtividades(cliente.getIdCliente()));
            
            //Telefone
            pessoaJuridica.setTelefones(telefones);
            cliente.setPessoaJuridica(pessoaJuridica);
            
        }
        else{
            PessoaFisica pessoaFisica = new PessoaFisica();
            // CPF
            CPF cpf = new CPF();
            cpf.setCpfString(resultado.getString("cpf"));
            pessoaFisica.setCpf(cpf);
            
            // RG
            RG rg = new RG();
            rg.setDataEmissão(resultado.getDate("dataEmissaoRG"));
            rg.setNumero(resultado.getString("rg"));
            pessoaFisica.setRg(rg);
            
            // Genero 
            ColGenero colGenero = new ColGenero(conexao);
            Genero genero = new Genero();
            genero.setId(resultado.getInt("idGenero"));
            genero = colGenero.Read(genero.getId());
            pessoaFisica.setGenero(genero);
            
            // Nome da Pessoa
            NomePessoaFisica nome = new NomePessoaFisica();
            nome.setPrimeiroNome(resultado.getString("PrimeiroNome"));
            nome.setNomeDoMeio(resultado.getString("NomeDoMeio"));
            nome.setSobrenome(resultado.getString("Sobrenome"));
            pessoaFisica.setNome(nome);

            // Endereço
            pessoaFisica.setEnderecoPrincipal(enderecoEspecifico);
            
            //Telefone
            pessoaFisica.setTelefones(telefones);
            cliente.setPessoaFisica(pessoaFisica);
        }
        
        
        return cliente;
    }
    
    public Cliente Create (Cliente cliente) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO CLIENTE ("
                                        // ID
                                        + "idCliente,"
                                        // CNPJ
                                        + " cnpj, "             // 1
                                        + "dataEmissaoCNPJ, "   // 2
                                        // CPF
                                        + "cpf, "               // 3
                                        // RG
                                        + "rg, "                // 4
                                        + "dataEmissaoRG, "     // 5
                                        + "idOrgaoExpeditorRG, "// 6 
                                        // ENDERECO
                                        + "idEndereco, "        // 7
                                        + "complementoEndereco, "//8
                                        + "nroEndereco, "       //9
                                        // GENERO
                                        + "idGenero, "          // 10
                                        // NOME PESSOA FISICA
                                        + "PrimeiroNome, "      // 11
                                        + "NomeDoMeio, "        // 12
                                        + "Sobrenome, "         // 13
                                        // NOME PESSOA JURIDICA
                                        + "NomeFantasia, "      // 14
                                        + "NomeEmpresa)"        // 15

                + " VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        
        
        if(cliente.getPessoaFisica()!= null){
            PessoaFisica pessoa = cliente.getPessoaFisica();
            // CPF
            conexao.getDeclaracao().setString(3, pessoa.getCpf().getCpfString());
            
            // RG
            conexao.getDeclaracao().setString(4,pessoa.getRg().getNumero());
            conexao.getDeclaracao().setDate(5, (Date) pessoa.getRg().getDataEmissão());
            conexao.getDeclaracao().setInt(6, pessoa.getRg().getOrgaoExpeditor().getIdOrgaoExpeditor());
            
            // Endereco
            SetEndereco(pessoa.getEnderecoPrincipal());
            
            // Genero
            conexao.getDeclaracao().setInt(10, pessoa.getSexo().getId());
            
            // Nome
            conexao.getDeclaracao().setString(11, pessoa.getNome().getPrimeiroNome());
            conexao.getDeclaracao().setString(12, pessoa.getNome().getNomeDoMeio());
            conexao.getDeclaracao().setString(13, pessoa.getNome().getSobrenome());
            
            // Telefones
            SetTelefones(pessoa.getTelefones());
            
            // Campos nulos
            conexao.getDeclaracao().setString(1, null);
            conexao.getDeclaracao().setDate(2, null);
            conexao.getDeclaracao().setString(14, null);
            conexao.getDeclaracao().setString(15, null);
            
        }   
        else if(cliente.getPessoaJuridica() != null){
            PessoaJuridica pessoa = cliente.getPessoaJuridica();
            // CNPJ
            conexao.getDeclaracao().setString(1, pessoa.getCnpj().toString());
            conexao.getDeclaracao().setDate(2, (Date) pessoa.getCnpj().getDataEmissao());
            
            // Endereco
            SetEndereco(pessoa.getEnderecoPrincipal());
            
            // Nome pessoa Juridica
            conexao.getDeclaracao().setString(14, pessoa.getNomeJ().getNomeFantasia()); // Nome Fantasia
            conexao.getDeclaracao().setString(15, pessoa.getNomeJ().getNomeEmpresa()); // Nome Empresa

            // Telefones
            
            // Atividades comerciais
            ColCliente_has_AtividadeComercial col = new ColCliente_has_AtividadeComercial(conexao);
            for(AtividadeComercial atividade:pessoa.getAtividadesComerciais()){
                col.Create(atividade.getId(), cliente.getIdCliente());
            }
            SetTelefones(pessoa.getTelefones());
            
            // Campos nulos
            conexao.getDeclaracao().setString(3, null);
            conexao.getDeclaracao().setString(4,null);
            conexao.getDeclaracao().setDate(5, null);
            conexao.getDeclaracao().setNull(6, java.sql.Types.INTEGER);
            conexao.getDeclaracao().setString(10, null);
            conexao.getDeclaracao().setString(11, null);
            conexao.getDeclaracao().setString(12, null);
            conexao.getDeclaracao().setString(13, null);
        }
        
        conexao.getDeclaracao().executeUpdate();
        cliente.setIdCliente(conexao.getLastInsertedId());
        return cliente;
    }
    
    public Cliente Read (int idCliente) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Cliente WHERE idCliente = ?");
        
        conexao.getDeclaracao().setInt(1, idCliente);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaCliente(resultado);
    }
    
    public Boolean Update (Cliente cliente) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome FROM Cliente SET (?) WHERE idCliente = ?");
        
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Cliente cliente) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Cliente WHERE idCliente = ?");
        conexao.getDeclaracao().setInt(1, cliente.getIdCliente());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Cliente> List() throws SQLException{
        ArrayList<Cliente> clientes = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cliente");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            clientes.add(PopulaCliente(resultado));
        }
                
        return clientes;
    }
}
