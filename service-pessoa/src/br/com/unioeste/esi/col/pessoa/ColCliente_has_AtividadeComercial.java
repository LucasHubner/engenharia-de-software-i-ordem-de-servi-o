/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author lucas
 */
public class ColCliente_has_AtividadeComercial {
    private Conexao conexao;

    public ColCliente_has_AtividadeComercial(Conexao conexao) {
        this.conexao = conexao;
    }
            
    public void Create(int idAtividadeComercial, int idCliente) throws SQLException{
        conexao.setDeclaracao("INSERT INTO Cliente_has_AtividadeComercial VALUES ?,?");
        conexao.getDeclaracao().setInt(1, idAtividadeComercial);
        conexao.getDeclaracao().setInt(2, idCliente);
        
        conexao.getDeclaracao().executeUpdate();

    }
    
    public ArrayList<AtividadeComercial> ListClientAtividades(int idCliente) throws SQLException{
        ArrayList<AtividadeComercial> atividades = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cliente_has_AtividadeComercial WHERE idCliente = ?");
        conexao.getDeclaracao().setInt(1, idCliente);
        ColAtividadeComercial col = new ColAtividadeComercial(conexao);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        while(resultado.next()){
                atividades.add(col.Read(resultado.getInt("idAtividadeComercial")));
        }

        return atividades;
    }

    
}
