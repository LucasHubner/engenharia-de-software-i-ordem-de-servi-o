/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.infra.bd.Conexao;

/**
 *
 * @author lucas
 */
public class ColAtividadeComercial {
    private Conexao conexao;

    public ColAtividadeComercial(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private AtividadeComercial PopulaAtividadeComercial(ResultSet resultado) throws SQLException{
        AtividadeComercial atividadeComercial = new AtividadeComercial();
        
        atividadeComercial.setId(resultado.getInt("idAtividadeComercial"));
        atividadeComercial.setNomeAtividadeComercial(resultado.getString("nome"));
        
        return atividadeComercial;
    }
    
    public AtividadeComercial Create (AtividadeComercial atividadeComercial) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO AtividadeComercial VALUES (null,?)");
        
        conexao.getDeclaracao().setString(1, atividadeComercial.getNomeAtividadeComercial());
        conexao.getDeclaracao().executeUpdate();
        atividadeComercial.setId(conexao.getLastInsertedId());
        
        return atividadeComercial;
    }
    
    public AtividadeComercial Read (int idAtividadeComercial) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM AtividadeComercial WHERE idAtividadeComercial = ?");
        
        conexao.getDeclaracao().setInt(1, idAtividadeComercial);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaAtividadeComercial(resultado);
    }
    
    public Boolean Update (AtividadeComercial atividadeComercial) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome FROM AtividadeComercial SET (?) WHERE idAtividadeComercial = ?");
        
        conexao.getDeclaracao().setString(1, atividadeComercial.getNomeAtividadeComercial());
        conexao.getDeclaracao().setInt(2, atividadeComercial.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (AtividadeComercial atividadeComercial) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM AtividadeComercial WHERE idAtividadeComercial = ?");
        
        conexao.getDeclaracao().setInt(1, atividadeComercial.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<AtividadeComercial> List() throws SQLException{
        ArrayList<AtividadeComercial> atividades = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM AtividadeComercial");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            atividades.add(PopulaAtividadeComercial(resultado));
        }
                
        return atividades;
    }
}