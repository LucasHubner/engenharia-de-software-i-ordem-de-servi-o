/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.bo.pessoa.fisica.Genero;

/**
 *
 * @author lucas
 */
public class ColGenero {
    private Conexao conexao;

    public ColGenero(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Genero PopulaGenero(ResultSet resultado) throws SQLException{
        Genero genero = new Genero();
        
        genero.setId(resultado.getInt("idGenero"));
        genero.setNome(resultado.getString("nome"));
        genero.setSigla(resultado.getString("sigla"));
        
        return genero;
    }
    
    public Genero Create (Genero genero) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO Genero VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, genero.getNome());
        conexao.getDeclaracao().setString(2, genero.getSigla());
        conexao.getDeclaracao().executeUpdate();
        genero.setId(conexao.getLastInsertedId());
        
        return genero;
    }
    
    public Genero Read (int idGenero) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Genero WHERE idGenero = ?");
        
        conexao.getDeclaracao().setInt(1, idGenero);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        if(!resultado.isBeforeFirst()){
            return new Genero();
        }
        resultado.next();
        
        return PopulaGenero(resultado);
    }
    
    public Boolean Update (Genero genero) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeGenero,siglaGenero FROM Genero SET (?,?) WHERE idGenero = ?");
        
        conexao.getDeclaracao().setString(1, genero.getNome());
        conexao.getDeclaracao().setString(2, genero.getSigla());
        conexao.getDeclaracao().setInt(3, genero.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Genero genero) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Genero WHERE idGenero = ?");
        
        conexao.getDeclaracao().setInt(1, genero.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Genero> List() throws SQLException{
        ArrayList<Genero> generos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Genero");
        ResultSet resultado =conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            generos.add(PopulaGenero(resultado));
        }
                
        return generos;
    }
}