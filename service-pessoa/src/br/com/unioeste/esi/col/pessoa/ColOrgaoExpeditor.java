/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.pessoa;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.bo.pessoa.fisica.OrgaoExpeditor;
import br.com.unioeste.esi.col.endereco.ColUnidadeFederativa;

/**
 *
 * @author lucas
 */
public class ColOrgaoExpeditor {
    private Conexao conexao;

    public ColOrgaoExpeditor(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private OrgaoExpeditor PopulaOrgaoExpeditor(ResultSet resultado) throws SQLException{
        OrgaoExpeditor orgaoExpeditor = new OrgaoExpeditor();
        orgaoExpeditor.setIdOrgaoExpeditor(resultado.getInt("idOrgaoExpeditor"));
        orgaoExpeditor.setNomeCompleto(resultado.getString("nomeCompleto"));
        orgaoExpeditor.setNomeAbreviado(resultado.getString("nomeAbreviado"));
        ColUnidadeFederativa col = new ColUnidadeFederativa(conexao);
        orgaoExpeditor.setUf(col.Read(resultado.getInt("idUnidadeFederativa")));
        
        
        return orgaoExpeditor;
    }
    
    public OrgaoExpeditor Create (OrgaoExpeditor orgaoExpeditor) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO OrgaoExpeditor"
                    + " (idOrgaoExpeditor,"
                    + " nomeCompleto,"
                    + " nomeAbreviado,"
                    + " idUnidadeFederativa) "
                + "VALUES (null,?,?,?)");
        conexao.getDeclaracao().setString(1, orgaoExpeditor.getNomeCompleto());
        conexao.getDeclaracao().setString(2, orgaoExpeditor.getNomeAbreviado());
        conexao.getDeclaracao().setInt(3, orgaoExpeditor.getUf().getIdUnidadeFederativa());

        conexao.getDeclaracao().executeUpdate();
        orgaoExpeditor.setIdOrgaoExpeditor(conexao.getLastInsertedId());
        
        return orgaoExpeditor;
    }
    
    public OrgaoExpeditor Read (int idOrgaoExpeditor) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM OrgaoExpeditor WHERE idOrgaoExpeditor = ?");
        
        conexao.getDeclaracao().setInt(1, idOrgaoExpeditor);
        ResultSet resultado =conexao.getDeclaracao().executeQuery();
        
        if (!resultado.isBeforeFirst()){
            return new OrgaoExpeditor();
        }
        resultado.next();
        
        return PopulaOrgaoExpeditor(resultado);
    }
    
    public Boolean Update (OrgaoExpeditor orgaoExpeditor) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeOrgaoExpeditor,siglaOrgaoExpeditor,idUnidadeFederativa FROM OrgaoExpeditor SET (?,?,?) WHERE idOrgaoExpeditor = ?");
        
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (OrgaoExpeditor orgaoExpeditor) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM OrgaoExpeditor WHERE idOrgaoExpeditor = ?");
        conexao.getDeclaracao().setInt(1, orgaoExpeditor.getIdOrgaoExpeditor());
        
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<OrgaoExpeditor> List() throws SQLException{
        ArrayList<OrgaoExpeditor> orgaoExpeditors = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM OrgaoExpeditor");
        ResultSet resultado =conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            orgaoExpeditors.add(PopulaOrgaoExpeditor(resultado));
        }
                
        return orgaoExpeditors;
    }
}