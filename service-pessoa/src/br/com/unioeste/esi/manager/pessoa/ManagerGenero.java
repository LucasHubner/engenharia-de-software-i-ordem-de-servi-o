/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.fisica.Genero;
import br.com.unioeste.esi.col.pessoa.ColGenero;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerGenero {
    Conexao conexao;
    
    public void validar(Genero genero) throws PessoaException{
        if (genero.getNome()== null || genero.getNome().equals("")){
            throw new PessoaException("" + "Atributo 'Nome está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Genero CadastraGenero(Genero genero) throws SQLException, PessoaException{
        conexao = new Conexao();
        validar(genero);
        conexao.AbreConexao();
        ColGenero col = new ColGenero(conexao);
        genero = col.Create(genero);
        conexao.FechaConexao();
        return genero;
    }
    
    public Genero excluiGenero(Genero genero) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColGenero col = new ColGenero(conexao);
        col.Remove(genero);
        conexao.FechaConexao();
        return genero;
    }
    
    public ArrayList<Genero> listaGeneros() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColGenero col = new ColGenero(conexao);
        ArrayList<Genero> generos = col.List();
        conexao.FechaConexao();
        return generos;
    }
    
    public Genero recuperaGenero(int idGenero) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColGenero col = new ColGenero(conexao);
        Genero genero = col.Read(idGenero);
        conexao.FechaConexao();
        return genero;
    }
}
