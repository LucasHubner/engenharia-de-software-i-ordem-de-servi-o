/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import br.com.unioeste.esi.col.pessoa.ColAtividadeComercial;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerAtividadeComercial {
    Conexao conexao;
    
    public void validar(AtividadeComercial atividade) throws PessoaException{
        if (atividade.getNomeAtividadeComercial() == null || atividade.getNomeAtividadeComercial().equals("")){
            throw new PessoaException("" + "Atributo 'Nome está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public AtividadeComercial CadastraAtividadeComercial(AtividadeComercial atividade) throws SQLException, PessoaException{
        conexao = new Conexao();
        validar(atividade);
        conexao.AbreConexao();
        ColAtividadeComercial col = new ColAtividadeComercial(conexao);
        atividade = col.Create(atividade);
        conexao.FechaConexao();
        return atividade;
    }

    public AtividadeComercial excluiAtividadeComercial(AtividadeComercial atividade) throws SQLException {
        conexao = new Conexao();
        conexao.AbreConexao();
        ColAtividadeComercial col = new ColAtividadeComercial(conexao);
        col.Remove(atividade);
        conexao.FechaConexao();
        return atividade;
    }
    
    public ArrayList<AtividadeComercial> listaAtividadeComercial() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColAtividadeComercial col = new ColAtividadeComercial(conexao);
        ArrayList<AtividadeComercial> atividades = col.List();
        conexao.FechaConexao();
        return atividades;
    }

    public AtividadeComercial recuperaAtividadeComercial(int idAtividadeComercial) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColAtividadeComercial col = new ColAtividadeComercial(conexao);
        AtividadeComercial atividadeComercial = col.Read(idAtividadeComercial);
        conexao.FechaConexao();
        return atividadeComercial;
    }
}
