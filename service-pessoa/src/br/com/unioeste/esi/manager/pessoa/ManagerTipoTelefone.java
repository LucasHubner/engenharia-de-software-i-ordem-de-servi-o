/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.TipoTelefone;
import br.com.unioeste.esi.col.pessoa.ColTipoTelefone;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerTipoTelefone {
    Conexao conexao;
    
    public void validar(TipoTelefone tipo) throws PessoaException{
        if (tipo.getNome()== null || tipo.getNome().equals("")){
            throw new PessoaException("" + "Atributo 'Nome está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public TipoTelefone CadastraTipoTelefone(TipoTelefone tipoTelefone) throws SQLException, PessoaException{
        conexao = new Conexao();
        validar(tipoTelefone);
        conexao.AbreConexao();
        ColTipoTelefone col = new ColTipoTelefone(conexao);
        tipoTelefone = col.Create(tipoTelefone);
        conexao.FechaConexao();
        return tipoTelefone;
    }
    
    public ArrayList<TipoTelefone> listaTipoTelefones() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoTelefone col = new ColTipoTelefone(conexao);
        ArrayList<TipoTelefone> tipoTelefones = col.List();
        conexao.FechaConexao();
        return tipoTelefones;
    }
    
    public TipoTelefone recuperaTipoTelefone(int idTipoTelefone) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoTelefone col = new ColTipoTelefone(conexao);
        TipoTelefone tipoTelefone = new TipoTelefone();
        tipoTelefone.setId(idTipoTelefone);
        tipoTelefone = col.Read(tipoTelefone);
        conexao.FechaConexao();
        return tipoTelefone;
    }
}
