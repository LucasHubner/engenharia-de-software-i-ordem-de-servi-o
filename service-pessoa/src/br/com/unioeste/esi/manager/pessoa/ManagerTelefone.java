/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.bo.pessoa.Telefone;
import br.com.unioeste.esi.col.pessoa.ColTelefone;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerTelefone {
    Conexao conexao;
    ManagerTipoTelefone mTipoTelefone = new ManagerTipoTelefone();
    
    public void validar(Telefone telefone) throws PessoaException, SQLException{
        
        if (mTipoTelefone.recuperaTipoTelefone(telefone.getIdCliente()) == null){
            throw new PessoaException("Tipo Telefone não existente");
        }
        
        if (telefone.getTelefone() == 0){
            throw new PessoaException("" + "Atributo 'Número' está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Telefone CadastraTelefone(Telefone telefone) throws SQLException, PessoaException{
        conexao = new Conexao();
        validar(telefone);
        conexao.AbreConexao();
        ColTelefone col = new ColTelefone(conexao);
        telefone = col.Create(telefone);
        conexao.FechaConexao();
        return telefone;
    }
    
    public ArrayList<Telefone> listaTelefonesByCliente(Cliente cliente) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTelefone col = new ColTelefone(conexao);
        ArrayList<Telefone> telefones = col.ListByClient(cliente.getIdCliente());
        conexao.FechaConexao();
        return telefones;
    }
    
    public Telefone recuperaTelefone(int idTelefone) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTelefone col = new ColTelefone(conexao);
        Telefone telefone = new Telefone();
        telefone.setIdCliente(idTelefone);
        telefone = col.Read(telefone);
        conexao.FechaConexao();
        return telefone;
    }
}
