/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.fisica.OrgaoExpeditor;
import br.com.unioeste.esi.col.pessoa.ColOrgaoExpeditor;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.manager.endereco.ManagerUnidadeFederativa;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerOrgaoExpeditor {
    Conexao conexao;
    ManagerUnidadeFederativa mUF = new ManagerUnidadeFederativa();
    
    public void validar(OrgaoExpeditor orgaoExpeditor) throws PessoaException, SQLException{
        if(mUF.recuperaUnidadeFederativa(orgaoExpeditor.getUf().getIdUnidadeFederativa()) == null){
            throw new PessoaException("Unidade Federativa não existente");
        }
        
        if(orgaoExpeditor.getNomeCompleto() == null || orgaoExpeditor.getNomeCompleto().equals("")){
            throw new PessoaException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public OrgaoExpeditor CadastraOrgaoExpeditor(OrgaoExpeditor orgaoExpeditor) throws SQLException, PessoaException{
        conexao = new Conexao();
        validar(orgaoExpeditor);
        conexao.AbreConexao();
        ColOrgaoExpeditor col = new ColOrgaoExpeditor(conexao);
        orgaoExpeditor = col.Create(orgaoExpeditor);
        conexao.FechaConexao();
        return orgaoExpeditor;
    }
    
    public OrgaoExpeditor excluiOrgaoExpeditor(OrgaoExpeditor orgaoExpeditor) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrgaoExpeditor col = new ColOrgaoExpeditor(conexao);
        col.Remove(orgaoExpeditor);
        conexao.FechaConexao();
        return orgaoExpeditor;
    }
    
    public ArrayList<OrgaoExpeditor> listaOrgaoExpeditors() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrgaoExpeditor col = new ColOrgaoExpeditor(conexao);
        ArrayList<OrgaoExpeditor> orgaoExpeditors = col.List();
        conexao.FechaConexao();
        return orgaoExpeditors;
    }

    public OrgaoExpeditor recuperaOrgaoExpeditor(int idOrgaoExpeditor) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrgaoExpeditor col = new ColOrgaoExpeditor(conexao);
        OrgaoExpeditor orgaoExpeditor = col.Read(idOrgaoExpeditor);
        conexao.FechaConexao();
        return orgaoExpeditor;
    }
    
}
