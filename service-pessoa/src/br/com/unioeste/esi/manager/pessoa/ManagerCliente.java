/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.pessoa;

import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.col.pessoa.ColCliente;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.manager.pessoa.*;

/**
 *
 * @author lucas
 */
public class ManagerCliente {
    Conexao conexao;
    Cliente cliente;
    ManagerOrgaoExpeditor mOrgao;
    
    public void validar() throws PessoaException{
        if(cliente.getPessoaFisica() == null || cliente.getPessoaJuridica() == null){
            throw new PessoaException("" + "Cliente não encontrado");
        }
        
        if(cliente.getPessoaFisica() != null){
            if(cliente.getPessoaFisica().getCpf() == null){
                throw new PessoaException("" + "Atributo 'Cpf' está vazio" 
                                             + "Cliente não pode ser criado");
            }
            
            if(cliente.getPessoaFisica().getRg().getNumero() == null){
                throw new PessoaException("" + "Atributo 'Rg' está vazio"
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaFisica().getRg().getDataEmissão() == null){
                throw new PessoaException("" + "Atributo 'Data de emissão' está vazio"
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaFisica().getEnderecoPrincipal() == null){
                throw new PessoaException("" + "Atributo 'Endereco Específico' está vazio"
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaFisica().getNome().getPrimeiroNome() == null){
                throw new PessoaException("" + "Atributo 'Nome' está vazio"
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaFisica().getNome().getSobrenome() == null){
                throw new PessoaException("" + "Atributo 'Sobrenome' está vazio"
                                             + "Cliente não pode ser criado");
            }
        }
        
        if(cliente.getPessoaJuridica() != null){
            if(cliente.getPessoaJuridica().getCnpj() == null){
                throw new PessoaException("" + "Atributo 'CNPJ' está vazio" 
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaJuridica().getEnderecoPrincipal() == null){
                throw new PessoaException("" + "Atributo 'Endereço Específico' está vazio" 
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaJuridica().getNome().getPrimeiroNome() == null){
                throw new PessoaException("" + "Atributo 'Nome' está vazio" 
                                             + "Cliente não pode ser criado");
            }
            if(cliente.getPessoaJuridica().getNome().getSobrenome() == null){
                throw new PessoaException("" + "Atributo 'Sobrenome' está vazio" 
                                             + "Cliente não pode ser criado");
            }
        }
    }
    
    public Cliente CadastraCliente(Cliente cliente) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCliente col = new ColCliente(conexao);
        cliente = col.Create(cliente);
        conexao.FechaConexao();
        return cliente;
    }
    
    public Cliente excluiCliente(Cliente cliente) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCliente col = new ColCliente(conexao);
        col.Remove(cliente);
        conexao.FechaConexao();
        return cliente;
    }
    
    public ArrayList<Cliente> listaClientes() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCliente col = new ColCliente(conexao);
        ArrayList<Cliente> clientes = col.List();
        conexao.FechaConexao();
        return clientes;
    }
    
    public Cliente recuperaCliente(int idCliente) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCliente col = new ColCliente(conexao);
        Cliente cliente = col.Read(idCliente);
        conexao.FechaConexao();
        return cliente;
    }
}
