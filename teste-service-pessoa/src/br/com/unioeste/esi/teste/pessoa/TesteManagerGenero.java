package br.com.unioeste.esi.teste.pessoa;

import br.com.unioeste.esi.bo.pessoa.fisica.Genero;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerGenero;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author lucas
 */
public class TesteManagerGenero {
        
    public static void testCreate(){
        Genero genero = new Genero();
        genero.setNome("Feminino");
        genero.setSigla("F");
        ManagerGenero manager = new ManagerGenero();
        try {
            try {
                genero = manager.CadastraGenero(genero);
            } catch (PessoaException ex) {
                Logger.getLogger(TesteManagerGenero.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(genero.getId());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(TesteManagerGenero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ArrayList<Genero> generos = new ArrayList<>();
        ManagerGenero manager = new ManagerGenero();
        try {
            generos = manager.listaGeneros();
            for(Genero genero:generos){
                System.out.print(genero.getId());
                System.out.print(" - ");
                System.out.println(genero.getNome());
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(TesteManagerGenero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerGenero manager = new ManagerGenero();
        int idGenero = 2;
        try{
            Genero genero = manager.recuperaGenero(idGenero);
            if (genero == null) {
                System.out.println("null");
            }
            else{
                System.out.println(genero.getNome());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerGenero manager = new ManagerGenero();
        Genero genero = new Genero();
        genero.setId(3);
        try{
            manager.excluiGenero(genero);
            if (genero.getNome() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException{
//        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
