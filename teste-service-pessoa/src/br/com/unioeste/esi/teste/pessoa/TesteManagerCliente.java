/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.pessoa;

import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.bo.endereco.EnderecoEspecifico;
import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.bo.pessoa.NomePessoaFisica;
import br.com.unioeste.esi.bo.pessoa.Telefone;
import br.com.unioeste.esi.bo.pessoa.fisica.CPF;
import br.com.unioeste.esi.bo.pessoa.fisica.Genero;
import br.com.unioeste.esi.bo.pessoa.fisica.OrgaoExpeditor;
import br.com.unioeste.esi.bo.pessoa.fisica.PessoaFisica;
import br.com.unioeste.esi.bo.pessoa.fisica.RG;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerCliente;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class TesteManagerCliente {
        
    public static void testCreate() throws PessoaException{
        Cliente cliente = new Cliente();
        PessoaFisica fisica = new PessoaFisica();
        CPF cpf = new CPF();
        RG rg = new RG();
        OrgaoExpeditor orgaoExpeditor = new OrgaoExpeditor();
        NomePessoaFisica nome = new NomePessoaFisica();
        Genero genero = new Genero();
        Endereco endereco = new Endereco();
        ArrayList<Telefone> telefones = new ArrayList<>();
        EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico();
        genero.setId(1);
        java.util.Date utilData = new java.util.Date();
        utilData.setTime(3600000);
        java.sql.Date sqlDate = new java.sql.Date(utilData.getTime());
        rg.setDataEmissão(sqlDate);
        rg.setNumero("123456789");
        orgaoExpeditor.setIdOrgaoExpeditor(4);
        rg.setOrgaoExpeditor(orgaoExpeditor);
        cpf.setCpfString("0987654321");
        nome.setNomeCompleto("Joao da Silva");
        nome.setNomeAbreviado("JDS");
        nome.setNomeDoMeio("Da");
        nome.setPrimeiroNome("Joao");
        nome.setSobrenome("Silva");
        endereco.setIdEndereco(4);
        enderecoEspecifico.setEndereco(endereco);
        enderecoEspecifico.setComplementoEndereco("ap.0");
        enderecoEspecifico.setNroEndereco(1543);
        fisica.setCpf(cpf);
        fisica.setGenero(genero);
        fisica.setRg(rg);
        fisica.setNome(nome);
        fisica.setEnderecoPrincipal(enderecoEspecifico);
        fisica.setTelefones(telefones);
        cliente.setPessoaFisica(fisica);
        
        ManagerCliente manager = new ManagerCliente();
        try {
            cliente = manager.CadastraCliente(cliente);
            System.out.print(cliente.getIdCliente());
            System.out.print(" - ");
            System.out.println(cliente.getPessoaFisica().getNome().getNomeCompleto());

        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    
    public static void testList(){
       ArrayList<Cliente> clientes = new ArrayList<>();
       ManagerCliente manager = new ManagerCliente();
        try {
            clientes = manager.listaClientes();
            for(Cliente cliente:clientes){
                if (cliente.getPessoaFisica() == null){
                    System.out.print("Pessoa juridica: ");
                }
                if (cliente.getPessoaJuridica() == null){
                    System.out.print("Pessoa fisica: ");
                }
                System.out.print(cliente.getIdCliente());
                System.out.print(" - ");
                // Tem que arrumar essa parada mano, e se for pessoa Juridica? se for juridica no vai pegar.
                // Tem que ver tbm se ta vindo erro de SQL.
                System.out.println(cliente.getPessoaFisica().getNome().getPrimeiroNome());
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(TesteManagerCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerCliente manager = new ManagerCliente();
        int idCliente = 3;
        try{
            Cliente cliente = manager.recuperaCliente(idCliente);
            if (cliente == null) {
                System.out.println("null");
            }
            else{
                if (cliente.getPessoaFisica() == null){
                    System.out.print("Pessoa juridica: ");
                }
                if (cliente.getPessoaJuridica() == null){
                    System.out.print("Pessoa fisica: ");
                }
                System.out.println(cliente.getPessoaFisica().getNome().getPrimeiroNome());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerCliente manager = new ManagerCliente();
        Cliente cliente = new Cliente();
        cliente.setIdCliente(3);
        try{
            manager.excluiCliente(cliente);
            System.out.println("ok");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) throws PessoaException, SQLException{
//        testCreate();
//        testList();
//        testRecupera();
        testExclui();
    }
}
