/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.pessoa;

import br.com.unioeste.esi.bo.pessoa.TipoTelefone;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerTipoTelefone;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class TesteManagerTipoTelefone {
    
    public static void testCreate() throws PessoaException{
        TipoTelefone tipo = new TipoTelefone();
        tipo.setNome("Celular");

        ManagerTipoTelefone manager = new ManagerTipoTelefone();
        try {
            tipo = manager.CadastraTipoTelefone(tipo);
            System.out.print(tipo.getId());
            System.out.print(" - ");
            System.out.println(tipo.getNome());

        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerTipoTelefone.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    
    public static void testList(){
       ArrayList<TipoTelefone> tipos = new ArrayList<>();
       ManagerTipoTelefone manager = new ManagerTipoTelefone();
        try {
            tipos = manager.listaTipoTelefones();
            for(TipoTelefone tipo:tipos){
                System.out.print(tipo.getId());
                System.out.print(" - ");
                System.out.println(tipo.getNome());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TesteManagerTipoTelefone.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerTipoTelefone manager = new ManagerTipoTelefone();
        int idTipoTelefone = 1;
        try{
            TipoTelefone tipoTelefone = manager.recuperaTipoTelefone(idTipoTelefone);
            if (tipoTelefone == null) {
                System.out.println("null");
            }
            else{
                System.out.println(tipoTelefone.getNome());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) throws PessoaException, SQLException{
//        testCreate();
//        testList();
        testRecupera();
    }
}