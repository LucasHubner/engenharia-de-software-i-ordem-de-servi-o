package br.com.unioeste.esi.teste.pessoa;

import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.bo.pessoa.fisica.OrgaoExpeditor;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerOrgaoExpeditor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author lucas
 */
public class TesteManagerOrgaoExpeditor {
        
    public static void testCreate() throws PessoaException{
        OrgaoExpeditor orgao = new OrgaoExpeditor();
        orgao.setNomeAbreviado("II");
        orgao.setNomeCompleto("Instituto de Identificacao");
        UnidadeFederativa uf = new UnidadeFederativa();
        // ID 2 é Paraná
        uf.setIdUnidadeFederativa(2);
        orgao.setUf(uf);
        ManagerOrgaoExpeditor manager = new ManagerOrgaoExpeditor();
        try {
            orgao = manager.CadastraOrgaoExpeditor(orgao);
            System.out.print(orgao.getIdOrgaoExpeditor());
            System.out.print(" - ");
            System.out.println(orgao.getNomeAbreviado());

        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerOrgaoExpeditor.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    
    public static void testList(){
       ArrayList<OrgaoExpeditor> orgaos = new ArrayList<>();
       ManagerOrgaoExpeditor manager = new ManagerOrgaoExpeditor();
        try {
            orgaos = manager.listaOrgaoExpeditors();
            for(OrgaoExpeditor orgao:orgaos){
                System.out.print(orgao.getIdOrgaoExpeditor());
                System.out.print(" - ");
                System.out.println(orgao.getNomeAbreviado());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TesteManagerOrgaoExpeditor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerOrgaoExpeditor manager = new ManagerOrgaoExpeditor();
        int idOrgaoExpeditor = 6;
        try{
            OrgaoExpeditor orgaoExpeditor = manager.recuperaOrgaoExpeditor(idOrgaoExpeditor);
            if (orgaoExpeditor == null) {
                System.out.println("null");
            }
            else{
                System.out.println(orgaoExpeditor.getNomeCompleto());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerOrgaoExpeditor manager = new ManagerOrgaoExpeditor();
        OrgaoExpeditor orgaoExpeditor = new OrgaoExpeditor();
        orgaoExpeditor.setIdOrgaoExpeditor(6);
        try{
            manager.excluiOrgaoExpeditor(orgaoExpeditor);
            if (orgaoExpeditor.getNomeCompleto() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) throws PessoaException, SQLException{
//        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }
}
