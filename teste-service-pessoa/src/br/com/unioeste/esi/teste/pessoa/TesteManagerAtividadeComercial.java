/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.pessoa;

import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerAtividadeComercial;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro
 */
public class TesteManagerAtividadeComercial {
    public static void testCreate() throws PessoaException{
       String nome = "Unioeste";
       AtividadeComercial atividadeComercial = new AtividadeComercial();
       atividadeComercial.setNomeAtividadeComercial(nome);
        ManagerAtividadeComercial manager = new ManagerAtividadeComercial();
        try {
            atividadeComercial = manager.CadastraAtividadeComercial(atividadeComercial);
            System.out.print(atividadeComercial.getId());
            System.out.print(" - ");
            System.out.println(atividadeComercial.getNomeAtividadeComercial());

        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerAtividadeComercial.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    
    public static void testList(){
       ArrayList<AtividadeComercial> atividadeComercials = new ArrayList<>();
       ManagerAtividadeComercial manager = new ManagerAtividadeComercial();
        try {
            atividadeComercials = manager.listaAtividadeComercial();
            for(AtividadeComercial atividadeComercial:atividadeComercials){
                System.out.print(atividadeComercial.getId());
                System.out.print(" - ");
                // Tem que arrumar essa parada mano, e se for pessoa Juridica? se for juridica no vai pegar.
                // Tem que ver tbm se ta vindo erro de SQL.
                System.out.println(atividadeComercial.getNomeAtividadeComercial());
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(TesteManagerAtividadeComercial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerAtividadeComercial manager = new ManagerAtividadeComercial();
        int idAtividadeComercial = 1;
        try{
            AtividadeComercial atividadeComercial = manager.recuperaAtividadeComercial(idAtividadeComercial);
            if (atividadeComercial == null) {
                System.out.println("null");
            }
            else{
                System.out.println(atividadeComercial.getNomeAtividadeComercial());
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerAtividadeComercial manager = new ManagerAtividadeComercial();
        AtividadeComercial atividadeComercial = new AtividadeComercial();
        atividadeComercial.setId(1);
        try{
            manager.excluiAtividadeComercial(atividadeComercial);
            System.out.println("ok");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) throws PessoaException, SQLException{
//        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }
}
