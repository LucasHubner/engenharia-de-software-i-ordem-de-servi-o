/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.infra.bd;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import java.sql.Statement;
import javax.naming.InitialContext;

 
public class Conexao implements Serializable {
    private Connection conexao;
    private PreparedStatement declaracao;
   
     
    
    public boolean AbreConexao(){
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            InitialContext ctx = new InitialContext();
            javax.sql.DataSource dataSource = (javax.sql.DataSource)ctx.lookup("jdbc/os");
            conexao = dataSource.getConnection();
            conexao.setAutoCommit(false);
            
            return true;
        } catch (NamingException | SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean FechaConexao(){
        try{
            conexao.commit();
            conexao.close();
            return true;
        }
        catch(SQLException e){
            System.out.print(e);
            return false;
        }
    }

    
    public Connection getConexao() {
        return conexao;
    }

    public void setConexao(Connection conexao) {
        this.conexao = conexao;
    }

    public PreparedStatement getDeclaracao() {
        return declaracao;
    }

    public void setDeclaracao(String declaracao) throws SQLException {
        this.declaracao = this.conexao.prepareStatement(declaracao, Statement.RETURN_GENERATED_KEYS);
        
    }
    public int getLastInsertedId() throws SQLException{
        ResultSet resultado = declaracao.getGeneratedKeys();
        resultado.next();
        return resultado.getInt(1);
    }


}
