/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.bo.endereco.Cep;
import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerEndereco;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerEndereco {
        
    public static void testCreate() throws EnderecoException{
        Endereco endereco = new Endereco();
        ManagerEndereco manager = new ManagerEndereco();

        Cep cep = new Cep();
        cep.setCepString("85960-000");
        endereco.setCep(cep);

        Logradouro logradouro = new Logradouro();
        logradouro.setIdLogradouro(1);
        endereco.setLogradouro(logradouro);
        
        // No existe nenhum bairro cadastrado no b.d., ento no tem como 
        //eu sei, só que eu quero que ele dispare o meu exception, eu fiz pra isso, e ele não ta indo
        
        Bairro bairro = new Bairro();
        bairro.setIdBairro(4);
        endereco.setBairro(bairro);
        
        Pais pais = new Pais();
        pais.setIdPais(1);
        endereco.setPais(pais);
        
        Cidade cidade = new Cidade();
        cidade.setIdCidade(1);
        endereco.setCidade(cidade);
        
        try {
            endereco = manager.CadastraEndereco(endereco);
            System.out.print(endereco.getIdEndereco());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerEndereco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerEndereco manager = new ManagerEndereco();

        try {
            ArrayList<Endereco> enderecos = manager.listaEnderecos();
            for(Endereco endereco:enderecos){
                System.out.println("" + endereco.getLogradouro().getNomeLogradouro() + " "
                                      + endereco.getBairro().getNomeBairro() + " "
                                      + endereco.getCidade().getNomeCidade() + " " 
                                      + endereco.getCidade().getUnidadeFederativa().getSiglaUnidadeFederativa() + " "
                                      + endereco.getPais().getNomePais() + " "
                                      + endereco.getCep().getCepString());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCidade.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerEndereco manager = new ManagerEndereco();
        int idEndereco = 2;
        try{
            Endereco endereco = manager.recuperaEndereco(idEndereco);
            if (endereco == null) {
                System.out.println("null");
            }
            else{
                System.out.println("" + endereco.getLogradouro().getNomeLogradouro() + " , "
                                      + endereco.getBairro().getNomeBairro() + " / "
                                      + endereco.getCidade().getNomeCidade() + " - " 
                                      + endereco.getCidade().getUnidadeFederativa().getSiglaUnidadeFederativa() + " - "
                                      + endereco.getPais().getNomePais() + " ("
                                      + endereco.getCep().getCepString() + ")");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerEndereco manager = new ManagerEndereco();
        Endereco endereco = new Endereco();
        endereco.setIdEndereco(5);
        try{
            manager.excluiEndereco(endereco);
            if (endereco.getLogradouro().getNomeLogradouro() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
   
    public static void main(String args[]) throws SQLException, EnderecoException{
//        testCreate();
//        testList();
//        testRecupera();
        testExclui();
    }

}
