/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerPais;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerPais {
        
    public static void testCreate() throws EnderecoException{
        ManagerPais manager = new ManagerPais();
        Pais pais = new Pais();
        pais.setNomePais("Argentina");
        pais.setSiglaPais("AR");
        try {
            pais = manager.CadastraPais(pais);
            System.out.println(pais.getIdPais());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerPais.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ArrayList<Pais> paises = new ArrayList<>();
        ManagerPais manager = new ManagerPais();

        try {
            paises = manager.listaPaises();
            for(Pais pais:paises){
                System.out.print(pais.getIdPais());
                System.out.print(" - ");
                System.out.println(pais.getNomePais());
            }
            
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerPais.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerPais manager = new ManagerPais();
        int idPais = 0;
        try{
            Pais pais = manager.recuperaPais(idPais);
            if (pais == null) {
                System.out.println("null");
            }
            else{
                System.out.println(pais.getNomePais());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerPais manager = new ManagerPais();
        Pais pais = new Pais();
        pais.setIdPais(1);
        try{
            manager.excluiPais(pais);
            if (pais.getNomePais() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    public static void main(String args[]) throws SQLException, EnderecoException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
