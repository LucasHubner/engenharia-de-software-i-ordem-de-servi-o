/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerTipoLogradouro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteTipoLogradouro {
        
    public static void testCreate() throws EnderecoException{
        ManagerTipoLogradouro manager = new ManagerTipoLogradouro();
        TipoLogradouro tipo = new TipoLogradouro();
        tipo.setNomeTipoLogradouro("Alameda");
        tipo.setSiglaTipoLogradouro("A.");
        try {
            tipo = manager.CadastraTipoLogradouro(tipo);
        } catch (SQLException ex) {
            Logger.getLogger(TesteTipoLogradouro.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(tipo.getIdTipoLogradouro());
    }
    
    public static void testList(){
        ArrayList<TipoLogradouro> tipoLogradouro = new ArrayList<>();
        ManagerTipoLogradouro manager = new ManagerTipoLogradouro();
        try {
            tipoLogradouro = manager.listaTipoLogradouros();
            for(TipoLogradouro tipo:tipoLogradouro){
                System.out.print(tipo.getIdTipoLogradouro());
                System.out.print(" - ");
                System.out.println(tipo.getNomeTipoLogradouro());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TesteTipoLogradouro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerTipoLogradouro manager = new ManagerTipoLogradouro();
        int idTipoLogradouro = 1;
        try{
            TipoLogradouro tipoLogradouro = manager.recuperaTipoLogradouro(idTipoLogradouro);
            if (tipoLogradouro == null) {
                System.out.println("null");
            }
            else{
                System.out.println(tipoLogradouro.getNomeTipoLogradouro());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerTipoLogradouro manager = new ManagerTipoLogradouro();
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        tipoLogradouro.setIdTipoLogradouro(1);
        try{
            manager.excluiTipoLogradouro(tipoLogradouro);
            if (tipoLogradouro.getNomeTipoLogradouro() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws EnderecoException, SQLException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
