/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerUnidadeFederativa;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerUnidadeFederativa {
        
    public static void testCreate() throws EnderecoException{
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();

        UnidadeFederativa uf = new UnidadeFederativa();
        uf.setNomeUnidadeFederativa("Paraná");
        uf.setSiglaUnidadeFederativa("PR");
        try {
            uf = manager.CadastraUnidadeFederativa(uf);
            System.out.println(uf.getIdUnidadeFederativa());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerUnidadeFederativa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();

        try {
            ArrayList<UnidadeFederativa> ufs = manager.listaUnidadeFederativas();
            for(UnidadeFederativa uf:ufs){
                System.out.print(uf.getIdUnidadeFederativa());
                System.out.print(" - ");
                System.out.println(uf.getNomeUnidadeFederativa());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerUnidadeFederativa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();
        int idUnidadeFederativa = 2;
        try{
            UnidadeFederativa uf = manager.recuperaUnidadeFederativa(idUnidadeFederativa);
            if (uf == null) {
                System.out.println("null");
            }
            else{
                System.out.println(uf.getNomeUnidadeFederativa());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();
        UnidadeFederativa uf = new UnidadeFederativa();
        uf.setIdUnidadeFederativa(1);
        try{
            manager.excluiUnidadeFederativa(uf);
            if (uf.getNomeUnidadeFederativa() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws EnderecoException, SQLException{
//        testCreate();
//        testList();
        testRecupera();
//        testExclui();
    }

}
