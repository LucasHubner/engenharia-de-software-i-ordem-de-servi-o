    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerLogradouro;
import br.com.unioeste.esi.manager.endereco.ManagerTipoLogradouro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerLogradouro {
        
    public static void testCreate() throws EnderecoException{
        ManagerLogradouro manager = new ManagerLogradouro();
        ManagerTipoLogradouro managerTipo = new ManagerTipoLogradouro();
        Logradouro logradouro = new Logradouro();
        TipoLogradouro tipo = new TipoLogradouro();
        tipo.setIdTipoLogradouro(2);
        logradouro.setNomeLogradouro("Rua das Piranhas");
        try {
            tipo = managerTipo.RecuperaTipoLogradouro(tipo);
            logradouro.setTipoLogradouro(tipo);
            logradouro = manager.CadastraLogradouro(logradouro);
            
        } catch (SQLException ex) {
            Logger.getLogger(TesteManagerLogradouro.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(logradouro.getIdLogradouro());
    }
    
    public static void testList(){
        ArrayList<Logradouro> logradouros = new ArrayList<>();
        ManagerLogradouro manager = new ManagerLogradouro();
        try {
            logradouros = manager.listaLogradouros(1);
            for(Logradouro logradouro:logradouros){
                System.out.print(logradouro.getIdLogradouro());
                System.out.print(" - ");
                System.out.println(logradouro.getNomeLogradouro());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TesteManagerLogradouro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerLogradouro manager = new ManagerLogradouro();
        int idLogradouro = 0;
        try{
            Logradouro logradouro = manager.recuperaLogradouro(idLogradouro);
            if (logradouro == null) {
                System.out.println("null");
            }
            else{
                System.out.println(logradouro.getNomeLogradouro());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerLogradouro manager = new ManagerLogradouro();
        Logradouro logradouro = new Logradouro();
        logradouro.setIdLogradouro(1);
        try{
            manager.excluiLogradouro(logradouro);
            if (logradouro.getNomeLogradouro() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String args[]) throws EnderecoException, SQLException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
