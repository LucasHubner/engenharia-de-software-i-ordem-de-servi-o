/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerCidade;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerCidade {
        
    public static void testCreate() throws EnderecoException{
        ManagerCidade manager = new ManagerCidade();

        Cidade cidade = new Cidade();
        cidade.setNomeCidade("Marechal Candido Rondon");
        UnidadeFederativa uf = new UnidadeFederativa();
        uf.setIdUnidadeFederativa(2);
        cidade.setUnidadeFederativa(uf);
        try {
            cidade = manager.CadastraCidade(cidade);
            System.out.println(cidade.getIdCidade());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCidade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerCidade manager = new ManagerCidade();

        try {
            UnidadeFederativa uf = new UnidadeFederativa();
            uf.setIdUnidadeFederativa(2);
            ArrayList<Cidade> cidades = manager.listaCidadesPorUf(uf);
            for(Cidade cidade:cidades){
                System.out.println(cidade.getNomeCidade());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerCidade.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerCidade manager = new ManagerCidade();
        int idCidade = 3;
        try{
            Cidade cidade = manager.recuperaCidade(idCidade);
            if (cidade == null) {
                System.out.println("null");
            }
            else{
                System.out.println(cidade.getNomeCidade());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerCidade manager = new ManagerCidade();
        Cidade cidade = new Cidade();
        cidade.setIdCidade(3);
        try{
            manager.excluiCidade(cidade);
            if (cidade.getNomeCidade() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws SQLException, EnderecoException{
        testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
