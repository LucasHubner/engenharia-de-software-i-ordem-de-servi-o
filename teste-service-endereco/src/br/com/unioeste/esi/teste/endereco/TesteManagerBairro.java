/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.teste.endereco;

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerBairro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lucas
 */
public class TesteManagerBairro {
        
    public static void testCreate() throws EnderecoException{
        ManagerBairro manager = new ManagerBairro();

        Bairro bairro = new Bairro();
        bairro.setNomeBairro("Vila C");
        try {
            bairro = manager.CadastraBairro(bairro);
            System.out.println(bairro.getIdBairro());
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerBairro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testList(){
        ManagerBairro manager = new ManagerBairro();

        try {
            ArrayList<Bairro> bairros = manager.listaBairros();
            for(Bairro bairro:bairros){
                System.out.println(bairro.getNomeBairro());
            }
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            Logger.getLogger(TesteManagerBairro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void testRecupera() throws SQLException{
        ManagerBairro manager = new ManagerBairro();
        int idBairro = 0;
        try{
            Bairro bairro = manager.recuperaBairro(idBairro);
            if (bairro == null) {
                System.out.println("null");
            }
            else{
                System.out.println(bairro.getNomeBairro());
            }
        }
        catch(Exception e){
        }
    }
    
    public static void testExclui() throws SQLException{
        ManagerBairro manager = new ManagerBairro();
        Bairro bairro = new Bairro();
        bairro.setIdBairro(3);
        try{
            manager.excluiBairro(bairro);
            if (bairro.getNomeBairro() == null){
                System.out.println("worked");
            }
            else{
                System.out.println("not");
            }
        }
        catch(Exception e){
        }
    }
    
    public static void main(String args[]) throws EnderecoException, SQLException{
//       testCreate();
//        testList();
//        testRecupera();
//        testExclui();
    }

}
