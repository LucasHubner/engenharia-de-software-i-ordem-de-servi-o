/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.veiculo;

import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColModelo {
    Conexao conexao;

    public ColModelo(Conexao conexao) {
        this.conexao = conexao;
    }
    private Modelo PopulaModelo(ResultSet resultado) throws SQLException{
        Modelo modelo = new Modelo();
        
        modelo.setId(resultado.getInt("idModelo"));
        modelo.setNome(resultado.getString("nome"));
        ColMarca colMarca = new ColMarca(conexao);
        Marca marca = new Marca();
        marca.setId(resultado.getInt("idMarca"));
        modelo.setMarca(colMarca.Read(marca));
        
        return modelo;
    }
    
    public Modelo Create(Modelo modelo) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Modelo` (`idModelo`, `nome`, `idMarca`) VALUES (NULL, ?,?)");
        
        conexao.getDeclaracao().setString(1, modelo.getNome());
        conexao.getDeclaracao().setInt(2, modelo.getMarca().getId());
        
        conexao.getDeclaracao().executeUpdate();
        modelo.setId(conexao.getLastInsertedId());
        
        return modelo;
    }
    
    public Modelo Read (Modelo modelo) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Modelo WHERE idModelo = ?");
        
        conexao.getDeclaracao().setInt(1, modelo.getId());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaModelo(resultado);
    }
    
    public Boolean Update (Modelo modelo) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome, idMarca FROM Modelo SET (?,?) WHERE idModelo = ?");
        
        conexao.getDeclaracao().setString(1, modelo.getNome());
        conexao.getDeclaracao().setInt(1, modelo.getMarca().getId());
        conexao.getDeclaracao().setInt(3, modelo.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Modelo modelo) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Modelo WHERE idModelo = ?");
        
        conexao.getDeclaracao().setInt(1, modelo.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Modelo> ListByMarca(int idMarca) throws SQLException{
        ArrayList<Modelo> modelos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Modelo WHERE idMarca = ?");
        conexao.getDeclaracao().setInt(1,idMarca);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            Modelo modelo = PopulaModelo(resultado);
            modelos.add(modelo);
        }
        
        return modelos;
    }
}
