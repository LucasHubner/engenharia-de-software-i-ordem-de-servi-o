/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.veiculo;

import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColCor {
      Conexao conexao;

    public ColCor(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Cor PopulaCor(ResultSet resultado) throws SQLException{
        Cor cor = new Cor();
        
        cor.setId(resultado.getInt("idCor"));
        cor.setNome(resultado.getString("nome"));
        
        return cor;
    }
    
    public Cor Create(Cor cor) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Cor` (`idCor`, `nome`) VALUES (NULL, ?)");
        conexao.getDeclaracao().setString(1, cor.getNome());
        
        
        conexao.getDeclaracao().executeUpdate();
        cor.setId(conexao.getLastInsertedId());
        
        return cor;
    }
    
    public Cor Read (Cor cor) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Cor WHERE idCor = ?");
        
        conexao.getDeclaracao().setInt(1, cor.getId());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        if (!resultado.isBeforeFirst()){
            return new Cor();
        }
        resultado.next();
        
        return PopulaCor(resultado);
    }
    
    public Boolean Update (Cor cor) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome FROM Cor SET (?) WHERE idCor = ?");
        
        conexao.getDeclaracao().setString(1, cor.getNome());
        conexao.getDeclaracao().setInt(2, cor.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Cor cor) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Cor WHERE idCor = ?");
        
        conexao.getDeclaracao().setInt(1, cor.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Cor> List() throws SQLException{
        ArrayList<Cor> cores = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cor");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            Cor cor = PopulaCor(resultado);
            cores.add(cor);
        }
        
        return cores;
    }
}
