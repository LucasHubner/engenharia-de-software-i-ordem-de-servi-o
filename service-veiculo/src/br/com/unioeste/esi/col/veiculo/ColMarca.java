/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.veiculo;

import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColMarca {
    Conexao conexao;

    public ColMarca(Conexao conexao) {
        this.conexao = conexao;
    }
    private Marca PopulaMarca(ResultSet resultado) throws SQLException{
        Marca marca = new Marca();
        
        marca.setId(resultado.getInt("idMarca"));
        marca.setNome(resultado.getString("nome"));
        
        return marca;
    }
    
    public Marca Create(Marca marca) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Marca` (`idMarca`, `nome`) VALUES (NULL, '"+marca.getNome()+"')");
        conexao.getDeclaracao().executeUpdate();
        marca.setId(conexao.getLastInsertedId());
        
        return marca;
    }
    
    public Marca Read (Marca marca) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Marca WHERE idMarca = ?");
        
        conexao.getDeclaracao().setInt(1, marca.getId());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        if (!resultado.isBeforeFirst()){
            return new Marca();
        }
        resultado.next();
        
        return PopulaMarca(resultado);
    }
    
    public Boolean Update (Marca marca) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeMarca FROM Marca SET (?) WHERE idMarca = ?");
        
        conexao.getDeclaracao().setString(1, marca.getNome());
        conexao.getDeclaracao().setInt(2, marca.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Marca marca) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Marca WHERE idMarca = ?");
        
        conexao.getDeclaracao().setInt(1, marca.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Marca> List() throws SQLException{
        ArrayList<Marca> marcas = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Marca");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            Marca marca = PopulaMarca(resultado);
            marcas.add(marca);
        }
        
        return marcas;
    }
}
