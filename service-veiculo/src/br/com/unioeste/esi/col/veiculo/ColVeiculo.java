/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.veiculo;

import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.bo.veiculo.Veiculo;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColVeiculo {
    Conexao conexao;

    public ColVeiculo(Conexao conexao) {
        this.conexao = conexao;
    }
    private Veiculo PopulaVeiculo(ResultSet resultado) throws SQLException{
        Veiculo veiculo = new Veiculo();
        veiculo.setPlaca(resultado.getString("placa"));
        ColCor colCor = new ColCor(conexao);
        
        veiculo.setChassi(resultado.getString("Chassi"));
        Cor cor = new Cor();
        cor.setId(resultado.getInt("idCor"));
        cor = colCor.Read(cor);
        veiculo.setCor(cor);
        
        ColModelo colModelo = new ColModelo(conexao);
        Modelo modelo = new Modelo();
        modelo.setId(resultado.getInt("idModelo"));
        modelo = colModelo.Read(modelo);
        veiculo.setModelo(modelo);
        
        veiculo.setAno(resultado.getInt("Ano"));
        
        return veiculo;
    }
    
    public Veiculo Create(Veiculo veiculo) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Veiculo` (placa, Chassi, Ano, idModelo, idCor) VALUES  (?, ?, ?, ?, ?)");
        conexao.getDeclaracao().setString(1, veiculo.getPlaca());
        conexao.getDeclaracao().setString(2, veiculo.getChassi());
        conexao.getDeclaracao().setInt(3, veiculo.getAno());
        conexao.getDeclaracao().setInt(4,veiculo.getModelo().getId());
        conexao.getDeclaracao().setInt(5,veiculo.getCor().getId());
        conexao.getDeclaracao().executeUpdate();
        
        
        return veiculo;
    }
    
    public Veiculo Read (Veiculo veiculo) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Veiculo WHERE placa = ?");
        
        conexao.getDeclaracao().setString(1, veiculo.getPlaca());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();  
        
        return PopulaVeiculo(resultado);
    }
    
    public Boolean Update (Veiculo veiculo) throws SQLException {
        
        conexao.setDeclaracao("UPDATE ( Chassi, Ano, idModelo, idCor)  FROM Veiculo SET ?, ?, ?, ? WHERE placa = ?");
        
        conexao.getDeclaracao().setString(1, veiculo.getChassi());
        conexao.getDeclaracao().setInt(2, veiculo.getAno());
        conexao.getDeclaracao().setInt(3,veiculo.getModelo().getId());
        conexao.getDeclaracao().setInt(4,veiculo.getCor().getId());
        conexao.getDeclaracao().setString(5, veiculo.getPlaca());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Veiculo veiculo) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Veiculo WHERE placa = ?");
        
        conexao.getDeclaracao().setString(1, veiculo.getPlaca());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Veiculo> List() throws SQLException{
        ArrayList<Veiculo> veiculos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Veiculo");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            Veiculo veiculo = PopulaVeiculo(resultado);
            veiculos.add(veiculo);
        }
        
        return veiculos;
    }
}
