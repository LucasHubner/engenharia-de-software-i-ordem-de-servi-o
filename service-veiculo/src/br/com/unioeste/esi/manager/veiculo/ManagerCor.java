/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.veiculo;

import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.col.veiculo.ColCor;
import br.com.unioeste.esi.exception.veiculo.VeiculoException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerCor {
    Conexao conexao;
    
    public void validar(Cor cor) throws VeiculoException{
        if(cor.getNome() == null || cor.getNome().equals("")){
            throw new VeiculoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Cor CadastraCor(Cor cor) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCor col = new ColCor(conexao);
        cor = col.Create(cor);
        conexao.FechaConexao();
        return cor;
    }
    
    public Cor excluiCor(Cor Cor) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCor col = new ColCor(conexao);
        col.Remove(Cor);
        conexao.FechaConexao();
        return Cor;
    }
    
    public ArrayList<Cor> listaCores() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCor col = new ColCor(conexao);
        ArrayList<Cor> Cors = col.List();
        conexao.FechaConexao();
        return Cors;
    }
    
    public Cor recuperaCor(int idCor) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCor col = new ColCor(conexao);
        Cor cor = new Cor();
        cor.setId(idCor);
        cor = col.Read(cor);
        conexao.FechaConexao();
        return cor;
    }
    
}
