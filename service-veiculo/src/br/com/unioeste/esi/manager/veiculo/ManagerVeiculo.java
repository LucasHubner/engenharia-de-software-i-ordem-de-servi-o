/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.veiculo;

import br.com.unioeste.esi.bo.veiculo.Veiculo;
import br.com.unioeste.esi.col.veiculo.ColVeiculo;
import br.com.unioeste.esi.exception.veiculo.VeiculoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerVeiculo {
    Veiculo veiculo;
    Conexao conexao;
    
    public void validar() throws VeiculoException{
        if (veiculo.getChassi() == null || veiculo.getChassi().equals("")){
            throw new VeiculoException("" + "Atributo 'Chassi' está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
        
        if(veiculo.getModelo() == null){
            throw new VeiculoException("" + "Atributo 'Modelo' está vazio. \n"
                                          + "Este atributo é de preenchimento obrigatório");
        }
        if(veiculo.getAno() == 0){
            throw new VeiculoException("" + "Atributo 'Ano' está vazio. \n"
                                          + "Este atributo é de preenchimento obrigatório");
        }
        if(veiculo.getCor() == null){
            throw new VeiculoException("" + "Atributo 'Cor' está vazio. \n"
                                          + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Veiculo CadastraVeiculo(Veiculo veiculo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColVeiculo col = new ColVeiculo(conexao);
        veiculo = col.Create(veiculo);
        conexao.FechaConexao();
        return veiculo;
    }
    
    public Veiculo excluiVeiculo(Veiculo veiculo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColVeiculo col = new ColVeiculo(conexao);
        col.Remove(veiculo);
        conexao.FechaConexao();
        return veiculo;
    }
    
    public ArrayList<Veiculo> listaVeiculos() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColVeiculo col = new ColVeiculo(conexao);
        ArrayList<Veiculo> veiculos = col.List();
        conexao.FechaConexao();
        return veiculos;
    }
    
    public Veiculo recuperaVeiculo(String placa) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColVeiculo col = new ColVeiculo(conexao);
        Veiculo marca = new Veiculo();
        marca.setPlaca(placa);
        marca = col.Read(marca);
        conexao.FechaConexao();
        return marca;
    }
}
