/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.veiculo;

import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.col.veiculo.ColModelo;
import br.com.unioeste.esi.exception.veiculo.VeiculoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerModelo {
    Modelo modelo;
    Conexao conexao;
    
    public void validar() throws VeiculoException{
        if (modelo.getNome()== null || modelo.getNome().equals("")){
            throw new VeiculoException("" + "Atributo 'Nome está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
        if(modelo.getMarca() == null){
            throw new VeiculoException("" + "Atributo 'Marca' está vazio. \n"
                                          + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Modelo CadastraModelo(Modelo modelo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColModelo col = new ColModelo(conexao);
        modelo = col.Create(modelo);
        conexao.FechaConexao();
        return modelo;
    }
    
    public Modelo excluiModelo(Modelo modelo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColModelo col = new ColModelo(conexao);
        col.Remove(modelo);
        conexao.FechaConexao();
        return modelo;
    }
    
    public ArrayList<Modelo> listaModelos(int idMarca) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColModelo col = new ColModelo(conexao);
        ArrayList<Modelo> modelos = col.ListByMarca(idMarca);
        conexao.FechaConexao();
        return modelos;
    }
    
    public Modelo recuperaModelo(int idModelo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColModelo col = new ColModelo(conexao);
        Modelo modelo = new Modelo();
        modelo.setId(idModelo);
        modelo = col.Read(modelo);
        conexao.FechaConexao();
        return modelo;
    }
}
