/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.veiculo;

import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.col.veiculo.ColMarca;
import br.com.unioeste.esi.exception.veiculo.VeiculoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerMarca {
    Marca marca;
    Conexao conexao;
    
    public void validar() throws VeiculoException{
        if (marca.getNome()== null || marca.getNome().equals("")){
            throw new VeiculoException("" + "Atributo 'Nome' está vazio. \n"
                                         + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Marca CadastraMarca(Marca marca) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColMarca col = new ColMarca(conexao);
        marca = col.Create(marca);
        conexao.FechaConexao();
        return marca;
    }
    
    public Marca excluiMarca(Marca marca) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColMarca col = new ColMarca(conexao);
        col.Remove(marca);
        conexao.FechaConexao();
        return marca;
    }
    
    public ArrayList<Marca> listaMarcas() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColMarca col = new ColMarca(conexao);
        ArrayList<Marca> marcas = col.List();
        conexao.FechaConexao();
        return marcas;
    }
    
    public Marca recuperaMarca(int idMarca) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColMarca col = new ColMarca(conexao);
        Marca marca = new Marca();
        marca.setId(idMarca);
        marca = col.Read(marca);
        conexao.FechaConexao();
        return marca;
    }
}
