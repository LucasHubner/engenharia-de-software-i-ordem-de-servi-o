/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.endereco;

import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerLogradouro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class LogradouroManagedBean {
    ManagerLogradouro manager;
    int idTipoLogradouro;
    String nomeLogradouro;
    ArrayList<Logradouro> logradouros;
    FacesContext context;
    
    /**
     * Creates a new instance of LogradouroManagedBean
     */
    public LogradouroManagedBean() {
        manager = new ManagerLogradouro();
        nomeLogradouro = new String();
        context = FacesContext.getCurrentInstance();
        logradouros = new ArrayList<>();
       
    }

    public ArrayList<Logradouro> getLogradouros() {
      
        return logradouros;
    }

    public void setLogradouros(ArrayList<Logradouro> logradouros) {
        this.logradouros = logradouros;
    }

    public int getIdTipoLogradouro() {
        return idTipoLogradouro;
    }

    public void setIdTipoLogradouro(int idTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
    }

    public String getNomeLogradouro() {
        return nomeLogradouro;
    }

    public void setNomeLogradouro(String nomeLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
    }
    public void AtualizaLogradouros(){
      try {
            logradouros = manager.listaLogradouros(idTipoLogradouro);
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );

            Logger.getLogger(LogradouroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraLogradouro() throws EnderecoException{
        manager = new ManagerLogradouro();
        Logradouro logradouro = new Logradouro();
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        tipoLogradouro.setIdTipoLogradouro(idTipoLogradouro);
        logradouro.setTipoLogradouro(tipoLogradouro);
        logradouro.setNomeLogradouro(nomeLogradouro);
        try {
            manager.CadastraLogradouro(logradouro);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Logradouro cadastrado com Sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            System.out.println(logradouro.getNomeLogradouro());
            System.out.println(logradouro.getTipoLogradouro().getIdTipoLogradouro());

            Logger.getLogger(LogradouroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
