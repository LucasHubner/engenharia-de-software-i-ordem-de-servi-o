/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.OrdemDeServico;

import javax.faces.bean.ManagedBean;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerEstadoOS;
import br.com.unioeste.esi.bo.OrdemDeServico.EstadoOS;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class EstadoOSManagedBean {
    EstadoOS estadoOS;
    FacesContext context;
    ArrayList<EstadoOS> estados;

    public ArrayList<EstadoOS> getEstados() {
        return estados;
    }

    public void setEstados(ArrayList<EstadoOS> estados) {
        this.estados = estados;
    }
    
    public EstadoOS getEstadoOS() {
        return estadoOS;
    }

    public void setEstadoOS(EstadoOS estadoOS) {
        this.estadoOS = estadoOS;
    }
    
    public void CadastraEstadoOS(){
        context = FacesContext.getCurrentInstance();
        try {
            new ManagerEstadoOS().CadastraEstadoOS(estadoOS);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Estado foi cadastrado!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(EstadoOSManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /**
     * Creates a new instance of EstadoOSManagedBean
     */
    public EstadoOSManagedBean() {
        this.estadoOS = new EstadoOS();
        try {
            this.estados = new ManagerEstadoOS().listaEstadoOSs();
        } catch (SQLException ex) {
            Logger.getLogger(EstadoOSManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
