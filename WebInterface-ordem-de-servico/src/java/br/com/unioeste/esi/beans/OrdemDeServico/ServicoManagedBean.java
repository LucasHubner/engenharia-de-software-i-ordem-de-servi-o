/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.Servico;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerServico;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class ServicoManagedBean {
    Servico servico;
    FacesContext context;

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }
    
    public void CadastraServico(){
        context = FacesContext.getCurrentInstance();

        try {
            new ManagerServico().CadastraServico(servico);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Servico foi cadastrado!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );

            Logger.getLogger(ServicoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Creates a new instance of ServicoManagedBean
     */
    public ServicoManagedBean() {
        servico = new Servico();
    }
    
}
