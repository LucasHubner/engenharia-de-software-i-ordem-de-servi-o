/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.veiculo;

import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.manager.veiculo.ManagerMarca;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class MarcaManagedBean {
    Marca marca;
    ArrayList<Marca> marcas;
    FacesContext context;

    public ArrayList<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(ArrayList<Marca> marcas) {
        this.marcas = marcas;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }
    
    /**
     * Creates a new instance of MarcaManagedBean
     */
    public MarcaManagedBean() {
        ManagerMarca manager = new ManagerMarca();
        marca = new Marca();
        try {
            marcas = manager.listaMarcas();
        } catch (SQLException ex) {
            Logger.getLogger(MarcaManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraMarca(){
        ManagerMarca manager = new ManagerMarca();
        context = FacesContext.getCurrentInstance();
        try {
            manager.CadastraMarca(marca);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Marca cadastrada com sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(MarcaManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
