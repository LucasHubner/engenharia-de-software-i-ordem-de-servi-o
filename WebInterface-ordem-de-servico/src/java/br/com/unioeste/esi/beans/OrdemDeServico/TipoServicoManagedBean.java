/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.TipoServico;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerTipoServico;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class TipoServicoManagedBean {
    TipoServico tipo;
    ArrayList<TipoServico> tipos;
    FacesContext context;

    public TipoServico getTipo() {
        return tipo;
    }

    public void setTipo(TipoServico tipo) {
        this.tipo = tipo;
    }

    public ArrayList<TipoServico> getTipos() {
        return tipos;
    }

    public void setTipos(ArrayList<TipoServico> tipos) {
        this.tipos = tipos;
    }
    
    public void CadastraTipoSevico(){
        context = FacesContext.getCurrentInstance();

        try {
            new  ManagerTipoServico().CadastraTipoServico(tipo);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Estado foi cadastrado!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(TipoServicoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates a new instance of TipoServicoManagedBean
     */
    public TipoServicoManagedBean() {
        this.tipo = new TipoServico();
        try {
            this.tipos = new ManagerTipoServico().listaTipoServicos();
        } catch (SQLException ex) {
            Logger.getLogger(TipoServicoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
