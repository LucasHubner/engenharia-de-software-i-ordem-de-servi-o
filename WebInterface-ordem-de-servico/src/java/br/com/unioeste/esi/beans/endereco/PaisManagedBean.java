/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.endereco;

import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerPais;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class PaisManagedBean {
    Pais pais;
    FacesContext context;
    ArrayList<Pais> paises;
    
    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public ArrayList<Pais> getPaises() {
        return paises;
    }

    public void setPaises(ArrayList<Pais> paises) {
        this.paises = paises;
    }
    
    /**
     * Creates a new instance of PaisManagedBean
     */
    public PaisManagedBean() {
        ManagerPais manager = new ManagerPais();
        paises = new ArrayList();
        pais = new Pais();
        context = FacesContext.getCurrentInstance();
        
        try {
            paises = manager.listaPaises();
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(PaisManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void CadastraPais() throws EnderecoException{
        ManagerPais manager = new ManagerPais();
        try {
            manager.CadastraPais(pais);
            context.addMessage(null, new FacesMessage("Sucesso!", "Pais cadastrado com sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(PaisManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
