/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.cliente;

import br.com.unioeste.esi.bo.pessoa.fisica.Genero;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerGenero;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class GeneroManagedBean {
    Genero genero;
    ArrayList<Genero> generos;
    FacesContext context;

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public ArrayList<Genero> getGeneros() {
        return generos;
    }

    public void setGeneros(ArrayList<Genero> generos) {
        this.generos = generos;
    }
    
    
    /**
     * Creates a new instance of GeneroManagedBean
     */
    public GeneroManagedBean() {
        genero = new Genero();
        try {
            generos = new ManagerGenero().listaGeneros();
        } catch (SQLException ex) {
            Logger.getLogger(GeneroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraGenero() throws PessoaException{
        context = FacesContext.getCurrentInstance();

        try {
            genero = new ManagerGenero().CadastraGenero(genero);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Genero foi cadastrado") );
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(GeneroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
