/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.endereco;

import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerUnidadeFederativa;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class UnidadeFederativaManagedBean {
    ArrayList<UnidadeFederativa> ufs;
    FacesContext context;
    UnidadeFederativa uf;
    /**
     * Creates a new instance of UnidadeFederativaManagedBean
     */
    public UnidadeFederativaManagedBean() {
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();
        context = FacesContext.getCurrentInstance();
        uf = new UnidadeFederativa();
        try {
            ufs = manager.listaUnidadeFederativas();
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(UnidadeFederativaManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public UnidadeFederativa getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativa uf) {
        this.uf = uf;
    }

    public ArrayList<UnidadeFederativa> getUfs() {
        return ufs;
    }

    public void setUfs(ArrayList<UnidadeFederativa> ufs) {
        this.ufs = ufs;
    }

    public FacesContext getContext() {
        return context;
    }

    public void setContext(FacesContext context) {
        this.context = context;
    }
    
    public void CadastraUnidadeFederativa() throws EnderecoException{
        ManagerUnidadeFederativa manager = new ManagerUnidadeFederativa();
        try {
            manager.CadastraUnidadeFederativa(uf);
            context.addMessage(null, new FacesMessage("Sucesso!", "Unidade Federativa cadastrada com sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(UnidadeFederativaManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
