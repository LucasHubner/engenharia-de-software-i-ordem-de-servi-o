/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.veiculo;

import br.com.unioeste.esi.bo.veiculo.Cor;
import br.com.unioeste.esi.manager.veiculo.ManagerCor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CorManagedBean {
    Cor cor;
    FacesContext context;
    ArrayList<Cor> cores;

    public ArrayList<Cor> getCores() {
        return cores;
    }

    public void setCores(ArrayList<Cor> cores) {
        this.cores = cores;
    }
    
    public Cor getCor() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }
    
    /**
     * Creates a new instance of CorManagedBean
     */
    public CorManagedBean() {
        ManagerCor manager = new ManagerCor();
        cores = new ArrayList<>();
        cor = new Cor();
        try {
            cores = manager.listaCores();
        } catch (SQLException ex) {
            Logger.getLogger(CorManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraCor(){
        context = FacesContext.getCurrentInstance();
        ManagerCor manager = new ManagerCor();
        try {
            manager.CadastraCor(cor);
           context.addMessage(null, new FacesMessage("Sucesso!",  "A cor foi cadastrada") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(CorManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
