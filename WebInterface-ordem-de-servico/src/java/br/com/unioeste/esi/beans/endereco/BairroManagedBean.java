package br.com.unioeste.esi.beans.endereco;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerBairro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class BairroManagedBean {
    String nomeBairro;
    ArrayList<Bairro> bairros;
    ManagerBairro manager;
    FacesContext context;
    Bairro bairro;

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }
    
    public ArrayList<Bairro> getBairros() {
        return bairros;
    }

    public void setBairros(ArrayList<Bairro> bairros) {
        this.bairros = bairros;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }
    
    public BairroManagedBean() {
        context = FacesContext.getCurrentInstance();
        manager = new ManagerBairro();
        nomeBairro = new String();
        try {
            bairros = manager.listaBairros();
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(BairroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
   
    public void ExcluirBairro(Bairro bairro){
        context = FacesContext.getCurrentInstance();

        try {
            new ManagerBairro().excluiBairro(bairro);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Bairro foi excluido") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(BairroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraBairro() throws EnderecoException {
        context = FacesContext.getCurrentInstance();
        Bairro bairro = new Bairro();
        bairro.setNomeBairro(nomeBairro);
        try {
            manager.CadastraBairro(bairro);
            
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Bairro foi cadastrado") );
        
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(BairroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
