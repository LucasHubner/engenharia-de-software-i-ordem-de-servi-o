package br.com.unioeste.esi.beans.endereco;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerCidade;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CidadeManagedBean {
    FacesContext context;
    Cidade cidade;
    UnidadeFederativa uf;
    ArrayList<Cidade> cidades;

    public ArrayList<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(ArrayList<Cidade> cidades) {
        this.cidades = cidades;
    }
    
    public int getIdUf() {
        return cidade.getUnidadeFederativa().getIdUnidadeFederativa();
    }

    public void setIdUf(int idUf) {
        cidade.getUnidadeFederativa().setIdUnidadeFederativa(idUf);
    }

    public String getNomeCidade() {
        return cidade.getNomeCidade();
    }

    public void setNomeCidade(String nomeCidade) {
        cidade.setNomeCidade(nomeCidade);
    }
    
    public CidadeManagedBean() {
        cidade = new Cidade();
        uf = new UnidadeFederativa();
        cidade.setUnidadeFederativa(uf);
        context = FacesContext.getCurrentInstance();
        cidades = new ArrayList<>();
    }
    
    public void OnUnidadeFederativaChange(){
        ManagerCidade manager = new ManagerCidade();
        try {
            cidades = manager.listaCidadesPorUf(cidade.getUnidadeFederativa());
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(CidadeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean cadastraCidade() throws EnderecoException{
        ManagerCidade manager = new ManagerCidade();
        try {
            manager.CadastraCidade(cidade);
            context.addMessage(null, new FacesMessage("Sucesso!","Cadastrado com Sucesso!"));
        } catch (SQLException | EnderecoException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(CidadeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
