/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.OrdemDeServico;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerOrdemDeServico;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class OrdemDeServicoManagedBean {
    OrdemDeServico os;
    ArrayList<OrdemDeServico> ordens;
    FacesContext context;

    public ArrayList<OrdemDeServico> getOrdens() {
        return ordens;
    }

    public void setOrdens(ArrayList<OrdemDeServico> ordens) {
        this.ordens = ordens;
    }

    public OrdemDeServico getOs() {
        return os;
    }

    public void setOs(OrdemDeServico os) {
        this.os = os;
    }
    
    
    public void CadastraOS(){
        context = FacesContext.getCurrentInstance();
        try {
            new ManagerOrdemDeServico().CadastraOrdemDeServico(os);
            context.addMessage(null, new FacesMessage("Sucesso!",  "A Ordem de Servico foi cadastrado!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(OrdemDeServicoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Creates a new instance of OrdemDeServicoManagedBean
     */
    public OrdemDeServicoManagedBean() {
        this.os = new OrdemDeServico();
        try {
            this.ordens = new ManagerOrdemDeServico().listaOrdemDeServicos();
        } catch (SQLException ex) {
            Logger.getLogger(OrdemDeServicoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
