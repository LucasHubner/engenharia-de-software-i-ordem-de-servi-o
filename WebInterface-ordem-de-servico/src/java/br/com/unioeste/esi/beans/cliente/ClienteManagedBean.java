/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.cliente;

import br.com.unioeste.esi.bo.pessoa.Cliente;
import br.com.unioeste.esi.manager.pessoa.ManagerCliente;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.com.unioeste.esi.bo.pessoa.fisica.PessoaFisica;
import br.com.unioeste.esi.bo.pessoa.juridica.PessoaJuridica;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class ClienteManagedBean {
    Cliente cliente;
    PessoaFisica pf;
    PessoaJuridica pj;

    public PessoaFisica getPf() {
        return pf;
    }

    public void setPf(PessoaFisica pf) {
        this.pf = pf;
    }

    public PessoaJuridica getPj() {
        return pj;
    }

    public void setPj(PessoaJuridica pj) {
        this.pj = pj;
    }
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    // Aqui eu to cadastrando a pessoa fisica, pj e igual, so muda que vc da um setPessoaJuridica(pj)
    public void CadastraPF(){
        cliente.setPessoaFisica(pf);
        try {
            new ManagerCliente().CadastraCliente(cliente);
        } catch (SQLException ex) {
            Logger.getLogger(ClienteManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CadastraPJ(){
        cliente.setPessoaJuridica(pj);
        try {
            new ManagerCliente().CadastraCliente(cliente);
        } catch (SQLException ex) {
            Logger.getLogger(ClienteManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Creates a new instance of ClienteManagedBean
     */
    public ClienteManagedBean() {
        cliente = new Cliente();
        pf = new PessoaFisica();
        pj = new PessoaJuridica();
    }
    
}
