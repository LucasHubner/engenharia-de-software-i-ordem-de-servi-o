/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.cliente;

import br.com.unioeste.esi.bo.pessoa.TipoTelefone;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.endereco.ManagerTipoLogradouro;
import br.com.unioeste.esi.manager.pessoa.ManagerTipoTelefone;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class TipoTelefoneManagedBean {
    TipoTelefone tipoTelefone;
    ArrayList<TipoTelefone> tiposTelefone;
    FacesContext context;

    public ArrayList<TipoTelefone> getTiposTelefone() {
        return tiposTelefone;
    }

    public void setTiposTelefone(ArrayList<TipoTelefone> tiposTelefone) {
        this.tiposTelefone = tiposTelefone;
    }
    
    public TipoTelefone getTipoTelefone() {
        return tipoTelefone;
    }

    public void setTipoTelefone(TipoTelefone tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }
    
    /**
     * Creates a new instance of TipoTelefoneManagedBean
     */
    public TipoTelefoneManagedBean() {
        tipoTelefone = new TipoTelefone();
        try {
            tiposTelefone = new ManagerTipoTelefone().listaTipoTelefones();
        } catch (SQLException ex) {
            Logger.getLogger(TipoTelefoneManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void CadastrarTipoTelefone() throws PessoaException{
        context = FacesContext.getCurrentInstance();

        try {
            new ManagerTipoTelefone().CadastraTipoTelefone(tipoTelefone);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Tipo de Telefone foi cadastrado") );
        } catch (SQLException | PessoaException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(TipoTelefoneManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
