/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.veiculo;

import br.com.unioeste.esi.bo.veiculo.Marca;
import br.com.unioeste.esi.bo.veiculo.Modelo;
import br.com.unioeste.esi.manager.veiculo.ManagerModelo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class ModeloManagedBean {
    Modelo modelo;
    int idMarca;
    FacesContext context;
    ArrayList<Modelo> modelos;

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }
    
    public Modelo getModelo() {
        return modelo;
     }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public ArrayList<Modelo> getModelos() {
        return modelos;
    }

    public void setModelos(ArrayList<Modelo> modelos) {
        this.modelos = modelos;
    }
    

    /**
     * Creates a new instance of ModeloManagedBean
     */
    public ModeloManagedBean() {
        modelo = new Modelo();
        modelo.setMarca(new Marca());
        modelos = new ArrayList<>();
    }
    
    public void OnMarcaChange(){
        ManagerModelo manager = new ManagerModelo();
        try {
            modelos = manager.listaModelos(idMarca);
        } catch (SQLException ex) {
            Logger.getLogger(ModeloManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void CadastraModelo(){
        ManagerModelo manager = new ManagerModelo();
        context = FacesContext.getCurrentInstance();

        try {
            manager.CadastraModelo(modelo);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Marca cadastrada com sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(ModeloManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
