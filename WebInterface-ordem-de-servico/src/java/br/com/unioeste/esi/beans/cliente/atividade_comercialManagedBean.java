/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.cliente;

import br.com.unioeste.esi.bo.pessoa.juridica.AtividadeComercial;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerAtividadeComercial;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class atividade_comercialManagedBean {
    AtividadeComercial atividadeComercial;
    ArrayList<AtividadeComercial> atividadesComerciais;
    FacesContext context;

    public AtividadeComercial getAtividadeComercial() {
        return atividadeComercial;
    }

    public void setAtividadeComercial(AtividadeComercial atividadeComercial) {
        this.atividadeComercial = atividadeComercial;
    }

    public ArrayList<AtividadeComercial> getAtividadesComerciais() {
        return atividadesComerciais;
    }

    public void setAtividadesComerciais(ArrayList<AtividadeComercial> atividadesComerciais) {
        this.atividadesComerciais = atividadesComerciais;
    }
    
    public void CadastraAtividadeComercial() throws PessoaException{
        context = FacesContext.getCurrentInstance();

        ManagerAtividadeComercial manager = new ManagerAtividadeComercial();
        try {
            manager.CadastraAtividadeComercial(atividadeComercial);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Atividade Comercial cadastrada com sucesso!") );

        } catch (SQLException|PessoaException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(atividade_comercialManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    /**
     * Creates a new instance of atividade_comercialManagedBean
     */
    public atividade_comercialManagedBean() {
        atividadeComercial = new AtividadeComercial();
        try {
            atividadesComerciais = new ManagerAtividadeComercial().listaAtividadeComercial();
        } catch (SQLException ex) {
            Logger.getLogger(atividade_comercialManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
