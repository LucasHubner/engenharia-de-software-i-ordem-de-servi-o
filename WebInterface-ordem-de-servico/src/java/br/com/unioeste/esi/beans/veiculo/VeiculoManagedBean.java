/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.veiculo;

import br.com.unioeste.esi.bo.veiculo.Veiculo;
import br.com.unioeste.esi.manager.veiculo.ManagerVeiculo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class VeiculoManagedBean {
    Veiculo veiculo;
    FacesContext context;
    ArrayList<Veiculo> veiculos;

    public ArrayList<Veiculo> getVeiculos() {
        return veiculos;
    }

    public void setVeiculos(ArrayList<Veiculo> veiculos) {
        this.veiculos = veiculos;
    }
    
    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public void CadastraVeiculo(){
        try {
            context = FacesContext.getCurrentInstance();
            new ManagerVeiculo().CadastraVeiculo(veiculo);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Veiculo cadastrado com sucesso!") );
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(VeiculoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Creates a new instance of VeiculoManagedBean
     */
    public VeiculoManagedBean() {
        veiculo = new Veiculo();
        try {
            veiculos = new ManagerVeiculo().listaVeiculos();
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
