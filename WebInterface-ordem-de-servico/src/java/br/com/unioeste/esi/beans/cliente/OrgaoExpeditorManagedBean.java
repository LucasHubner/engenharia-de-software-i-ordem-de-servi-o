/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.cliente;

import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.bo.pessoa.fisica.OrgaoExpeditor;
import br.com.unioeste.esi.exception.pessoa.PessoaException;
import br.com.unioeste.esi.manager.pessoa.ManagerOrgaoExpeditor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class OrgaoExpeditorManagedBean {
    OrgaoExpeditor orgao;
    ArrayList<OrgaoExpeditor> orgaosExpeditores;
    FacesContext context;

    public OrgaoExpeditor getOrgao() {
        return orgao;
    }

    public void setOrgao(OrgaoExpeditor orgao) {
        this.orgao = orgao;
    }

    public ArrayList<OrgaoExpeditor> getOrgaosExpeditores() {
        return orgaosExpeditores;
    }

    public void setOrgaosExpeditores(ArrayList<OrgaoExpeditor> orgaosExpeditores) {
        this.orgaosExpeditores = orgaosExpeditores;
    }

    
    /**
     * Creates a new instance of OrgaoExpeditorManagedBean
     */
    public OrgaoExpeditorManagedBean() {
        orgao = new OrgaoExpeditor();
        orgao.setUf(new UnidadeFederativa());
        orgaosExpeditores = new ArrayList<>();
        OnUnidadeFederativaChange();
    }
    
    public void OnUnidadeFederativaChange(){
        try {
            orgaosExpeditores = new ManagerOrgaoExpeditor().listaOrgaoExpeditors();
        } catch (SQLException ex) {
            Logger.getLogger(OrgaoExpeditorManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void CadastraOrgaoExpeditor() throws PessoaException{
        context = FacesContext.getCurrentInstance();

        try {
            new ManagerOrgaoExpeditor().CadastraOrgaoExpeditor(orgao);
            context.addMessage(null, new FacesMessage("Sucesso!",  "O Orgão Expeditor foi cadastrado!") );

        } catch (SQLException | PessoaException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(OrgaoExpeditorManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
