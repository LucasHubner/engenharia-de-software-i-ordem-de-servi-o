/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.beans.endereco;

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.bo.endereco.Cep;
import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerEndereco;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class EnderecoManagedBean {
    Endereco endereco;
    FacesContext context;

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    public void CadastraEndereco() throws EnderecoException{
        ManagerEndereco manager = new ManagerEndereco();
        System.out.println(endereco.getCidade().getIdCidade());
        System.out.println(endereco.getLogradouro().getIdLogradouro());
        context = FacesContext.getCurrentInstance();

        try {
            manager.CadastraEndereco(endereco);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Endereço cadastrado com sucesso!"));

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(EnderecoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /**
     * Creates a new instance of EnderecoManagedBean
     */
    
    public EnderecoManagedBean() {
        endereco = new Endereco();
        endereco.setCep(new Cep());
        endereco.setPais(new Pais());
        endereco.setBairro(new Bairro());
        endereco.setCidade(new Cidade());
        endereco.setLogradouro(new Logradouro());
        
        context = FacesContext.getCurrentInstance();

    }
    
}
