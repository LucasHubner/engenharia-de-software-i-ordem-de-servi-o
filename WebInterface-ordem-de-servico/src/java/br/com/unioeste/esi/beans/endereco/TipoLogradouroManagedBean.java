package br.com.unioeste.esi.beans.endereco;

import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.manager.endereco.ManagerTipoLogradouro;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */

@ManagedBean
@ViewScoped
public class TipoLogradouroManagedBean {
    int idTipoLogradouro;
    String nomeTipoLogradouro;
    String siglaTipoLogradouro;
    ArrayList<TipoLogradouro> tiposLogradouro;
    FacesContext context;
    ManagerTipoLogradouro manager;

    public TipoLogradouroManagedBean() {
        nomeTipoLogradouro = new String();
        siglaTipoLogradouro = new String();
        manager = new ManagerTipoLogradouro();
        context = FacesContext.getCurrentInstance();
        try {
            tiposLogradouro = manager.listaTipoLogradouros();
        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(TipoLogradouroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public String getSiglaTipoLogradouro() {
        return siglaTipoLogradouro;
    }

    public void setSiglaTipoLogradouro(String siglaTipoLogradouro) {
        this.siglaTipoLogradouro = siglaTipoLogradouro;
    }

    public ArrayList<TipoLogradouro> getTiposLogradouro() {
        return tiposLogradouro;
    }

    public void setTiposLogradouro(ArrayList<TipoLogradouro> tiposLogradouro) {
        this.tiposLogradouro = tiposLogradouro;
    }

    public int getIdTipoLogradouro() {
        return idTipoLogradouro;
    }

    public void setIdTipoLogradouro(int idTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
    }

    public String getNomeTipoLogradouro() {
        return nomeTipoLogradouro;
    }

    public void setNomeTipoLogradouro(String nomeTipoLogradouro) {
        this.nomeTipoLogradouro = nomeTipoLogradouro;
    }
    
    public void CadastraTipoLogradouro() throws EnderecoException{
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        ManagerTipoLogradouro manager = new ManagerTipoLogradouro();
        tipoLogradouro.setNomeTipoLogradouro(nomeTipoLogradouro);
        tipoLogradouro.setSiglaTipoLogradouro(siglaTipoLogradouro);
        try {
            manager.CadastraTipoLogradouro(tipoLogradouro);
            context.addMessage(null, new FacesMessage("Sucesso!",  "Tipo de Logradouro cadastrado com sucesso!") );

        } catch (SQLException ex) {
            context.addMessage(null, new FacesMessage("Erro!",  ex.getMessage()) );
            Logger.getLogger(TipoLogradouroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
