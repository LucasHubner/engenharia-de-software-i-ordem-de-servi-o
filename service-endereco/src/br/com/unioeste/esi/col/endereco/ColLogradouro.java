/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;

import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColLogradouro {
    
    private Conexao conexao;

    public ColLogradouro(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Logradouro PopulaLogradouro(ResultSet resultado) throws SQLException{
        Logradouro logradouro = new Logradouro();
        
        logradouro.setIdLogradouro(resultado.getInt("idLogradouro"));
        logradouro.setNomeLogradouro(resultado.getString("nomeLogradouro"));
        ColTipoLogradouro colTipoLogradouro = new ColTipoLogradouro(conexao);
        TipoLogradouro tipo = new TipoLogradouro();
        tipo.setIdTipoLogradouro(resultado.getInt("idTipoLogradouro"));
        logradouro.setTipoLogradouro(colTipoLogradouro.Read(tipo));

        return logradouro;
    }
    
    
    public Logradouro Create(Logradouro logradouro) throws SQLException{
        
        conexao.setDeclaracao("INSERT INTO `Logradouro` (`idLogradouro`, `nomeLogradouro`, `idTipoLogradouro`) "
                + "VALUES (NULL, '"+logradouro.getNomeLogradouro()+"', '"+logradouro.getTipoLogradouro().getIdTipoLogradouro()+"')");
        /*
            Atributos da tabela logradouro
                                -int idLogradouro -> Como o ID é AUTO INCREMENT, deixa null
                                -string nomeLogradouro
                                -int idTipoLogradouro
        */
      
        //Seta o nome logradouro
        //conexao.getDeclaracao().setString(1, logradouro.getNomeLogradouro());
        //Seta o id do TipoLogradouro, observe que a ID esta dentro do objeto TipoLogradouro
        // para isso, acessamos o objeto base Logradouro e depois pedimos ao metodo
        //getTipoLogradouro, com isso temos o objeto TipoLogradouro, e utilizamos seu metodo getIdTipoLogradouro
        //conexao.getDeclaracao().setInt(2, logradouro.getTipoLogradouro().getIdTipoLogradouro());
        
        conexao.getDeclaracao().executeUpdate();
        logradouro.setIdLogradouro(conexao.getLastInsertedId());
        
        return logradouro;
    }
    
    public Logradouro Read(int idLogradouro) throws SQLException {
        
        // SELECIONE TUDO DE logradouro ONDE o idLogradouro for IGUAL ao Parametro
        conexao.setDeclaracao("SELECT * FROM Logradouro WHERE  idLogradouro = ?");
        
        //Seta o parâmetro ? do select
        conexao.getDeclaracao().setInt(1, idLogradouro);
        
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        if(!resultado.isBeforeFirst()){
            return new Logradouro();
        }
        resultado.next();
        return PopulaLogradouro(resultado);
        
    }
    
    public Boolean Update(Logradouro logradouro) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeLogradouro FROM Logradouro SET (?) WHERE idLogradouro = ?");
        
        conexao.getDeclaracao().setString(1, logradouro.getNomeLogradouro());
        conexao.getDeclaracao().setInt(2, logradouro.getIdLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Logradouro logradouro) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Logradouro WHERE idLogradouro = ?");
        
        conexao.getDeclaracao().setInt(1, logradouro.getIdLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Logradouro> ListByTipoLogradouro(int idTipoLogradouro) throws SQLException{
    ArrayList<Logradouro> logradouros = new ArrayList<>();
    
    conexao.setDeclaracao("SELECT * FROM Logradouro WHERE idTipoLogradouro = ?");
    conexao.getDeclaracao().setInt(1, idTipoLogradouro);
    
    ResultSet resultado = conexao.getDeclaracao().executeQuery();
    while(resultado.next()){
        logradouros.add(PopulaLogradouro(resultado));
    }
    
    return logradouros;
    }
}