/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class ColCidade {
    
    private Conexao conexao;
    
    public ColCidade(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Cidade PopulaCidade(ResultSet resultado) throws SQLException{
        Cidade cidade = new Cidade();
        ColUnidadeFederativa colUnidadeFederativa = new ColUnidadeFederativa(conexao);
        
        cidade.setIdCidade(resultado.getInt("idCidade"));
        cidade.setNomeCidade(resultado.getString("nomeCidade"));
        cidade.setUnidadeFederativa(colUnidadeFederativa.Read(resultado.getInt("idUnidadeFederativa")));
        
        return cidade;
    }
    
    public Cidade Create (Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO Cidade VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, cidade.getNomeCidade());
        conexao.getDeclaracao().setInt(2, cidade.getUnidadeFederativa().getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        cidade.setIdCidade(conexao.getLastInsertedId());
        return cidade;
    }
    
    public Cidade Read (int idCidade) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Cidade WHERE idCidade = ? ");
        
        conexao.getDeclaracao().setInt(1, idCidade);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        if(!resultado.isBeforeFirst()){
            return new Cidade();
        }
        resultado.next();
        return PopulaCidade(resultado);
    }
    
    public Boolean Update (Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeCidade FROM Cidade SET (?) WHERE idCidade = ?");
        
        conexao.getDeclaracao().setString(1, cidade.getNomeCidade());
        conexao.getDeclaracao().setInt(2, cidade.getIdCidade());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Cidade cidade) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Cidade WHERE idCidade = ?");
        
        conexao.getDeclaracao().setInt(1, cidade.getIdCidade());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Cidade> ListByUnidadeFederativa(UnidadeFederativa uf) throws SQLException{
        ArrayList<Cidade> cidades = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cidade WHERE idUnidadeFederativa = ? ");
        conexao.getDeclaracao().setInt(1, uf.getIdUnidadeFederativa());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            cidades.add(PopulaCidade(resultado));
        }
        
        return cidades;
    }
}
