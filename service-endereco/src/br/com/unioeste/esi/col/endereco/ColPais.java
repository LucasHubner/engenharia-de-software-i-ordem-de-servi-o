/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;

import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColPais {
    
    private Conexao conexao;

    public ColPais(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Pais PopulaPais(ResultSet resultado) throws SQLException{
         
        Pais pais = new Pais();
        
        pais.setIdPais(resultado.getInt("idPais"));
        pais.setNomePais(resultado.getString("nomePais"));
        pais.setSiglaPais(resultado.getString("siglaPais"));
        
        return pais;
    }
    
    public Pais Create (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO Pais  VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, pais.getNomePais());
        conexao.getDeclaracao().setString(2, pais.getSiglaPais());
        
        conexao.getDeclaracao().executeUpdate();
        
        pais.setIdPais(conexao.getLastInsertedId());
        
        return pais;
    }
    
    public Pais Read (int idPais) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Pais WHERE idPais = ?");
        
        conexao.getDeclaracao().setInt(1, idPais);
        ResultSet resultado =conexao.getDeclaracao().executeQuery();
        if(!resultado.isBeforeFirst()){
            return new Pais();
        }
        resultado.next();
       
        return PopulaPais(resultado);
    }
    
    public Boolean Update (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomePais,siglaPais FROM Pais SET (?,?) WHERE idPais = ?");
        
        conexao.getDeclaracao().setString(1, pais.getNomePais());
        conexao.getDeclaracao().setString(2, pais.getSiglaPais());
        conexao.getDeclaracao().setInt(3, pais.getIdPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Pais WHERE idPais = ?");
        
        conexao.getDeclaracao().setInt(1, pais.getIdPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Pais> List() throws SQLException{
        ArrayList<Pais> paises = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Pais");
        
        ResultSet resultado =conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            paises.add(PopulaPais(resultado));
        }
        
        return paises;
        
    }
}
