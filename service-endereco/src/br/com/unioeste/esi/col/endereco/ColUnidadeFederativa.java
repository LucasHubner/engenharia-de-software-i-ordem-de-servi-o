/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class ColUnidadeFederativa {
    private Conexao conexao;

    public ColUnidadeFederativa(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private UnidadeFederativa PopulaUnidadeFederativa(ResultSet resultado) throws SQLException{
        UnidadeFederativa unidadeFederativa = new UnidadeFederativa();
        
        unidadeFederativa.setIdUnidadeFederativa(resultado.getInt("idUnidadeFederativa"));
        unidadeFederativa.setNomeUnidadeFederativa(resultado.getString("nomeUnidadeFederativa"));
        unidadeFederativa.setSiglaUnidadeFederativa(resultado.getString("siglaUnidadeFederativa"));
        
        return unidadeFederativa;
    }
    
    public UnidadeFederativa Create (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO UnidadeFederativa VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, unidadeFederativa.getNomeUnidadeFederativa());
        conexao.getDeclaracao().setString(2, unidadeFederativa.getSiglaUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        unidadeFederativa.setIdUnidadeFederativa(conexao.getLastInsertedId());
        
        return unidadeFederativa;
    }
    
    public UnidadeFederativa Read (int idUnidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM UnidadeFederativa WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setInt(1, idUnidadeFederativa);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        if (!resultado.isBeforeFirst()){
            return new UnidadeFederativa();
        }
        resultado.next();
        
        return PopulaUnidadeFederativa(resultado);
    }
    
    public Boolean Update (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeUnidadeFederativa, siglaUnidadeFederativa FROM UnidadeFederativa SET (?,?) WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setString(1, unidadeFederativa.getNomeUnidadeFederativa());
        conexao.getDeclaracao().setString(2, unidadeFederativa.getSiglaUnidadeFederativa());
        conexao.getDeclaracao().setInt(3, unidadeFederativa.getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM UnidadeFederativa WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setInt(1, unidadeFederativa.getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<UnidadeFederativa> List() throws SQLException{
        ArrayList<UnidadeFederativa> ufs = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM UnidadeFederativa");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            ufs.add(PopulaUnidadeFederativa(resultado));
        }
                
        return ufs;
    }
}