/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColBairro {
    Conexao conexao;
   
    public ColBairro(Conexao conexao) {
        this.conexao = conexao;
    }
     
    private Bairro PopulaBairro(ResultSet resultado) throws SQLException{
        Bairro bairro = new Bairro();
        bairro.setIdBairro(resultado.getInt("idBairro"));
        bairro.setNomeBairro(resultado.getString("nomeBairro"));
        return bairro;
    }
    
    public Bairro Create(Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Bairro` (`idBairro`, `nomeBairro`) VALUES (NULL, '"+bairro.getNomeBairro()+"')");
        conexao.getDeclaracao().executeUpdate();
        bairro.setIdBairro(conexao.getLastInsertedId());
        
        return bairro;
    }
    
    public Bairro Read (int idBairro) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Bairro WHERE idBairro = ?");
        
        conexao.getDeclaracao().setInt(1, idBairro);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.next();
        
        return PopulaBairro(resultado);
    }
    
    public Boolean Update (Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeBairro FROM Bairro SET (?) WHERE idBairro = ?");
        
        conexao.getDeclaracao().setString(1, bairro.getNomeBairro());
        conexao.getDeclaracao().setInt(2, bairro.getIdBairro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Bairro WHERE idBairro = ?");
        
        conexao.getDeclaracao().setInt(1, bairro.getIdBairro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Bairro> List() throws SQLException{
        ArrayList<Bairro> bairros = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Bairro");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            Bairro bairro = PopulaBairro(resultado);
            bairros.add(bairro);
        }
        
        return bairros;
    }
}
