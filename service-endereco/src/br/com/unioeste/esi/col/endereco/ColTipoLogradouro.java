/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.endereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.infra.bd.Conexao;

/**
 *
 * @author lucas
 */
public class ColTipoLogradouro {
    private Conexao conexao;

    public ColTipoLogradouro(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private TipoLogradouro PopulaTipoLogradouro(ResultSet resultado) throws SQLException{
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        
        tipoLogradouro.setIdTipoLogradouro(resultado.getInt("idTipoLogradouro"));
        tipoLogradouro.setNomeTipoLogradouro(resultado.getString("nomeTipoLogradouro"));
        tipoLogradouro.setSiglaTipoLogradouro(resultado.getString("siglaTipoLogradouro"));
        
        return tipoLogradouro;
    }
    
     public TipoLogradouro Create(TipoLogradouro tipoLogradouro) throws SQLException{
            
        conexao.setDeclaracao("INSERT INTO TipoLogradouro (idTipoLogradouro, nomeTipoLogradouro, siglaTipoLogradouro) VALUES (null, ?,?)");

        conexao.getDeclaracao().setString(1, tipoLogradouro.getNomeTipoLogradouro());
        conexao.getDeclaracao().setString(2, tipoLogradouro.getSiglaTipoLogradouro());
        conexao.getDeclaracao().executeUpdate();

        tipoLogradouro.setIdTipoLogradouro(conexao.getLastInsertedId());
        
        return tipoLogradouro;
    }
    public TipoLogradouro Read(TipoLogradouro tipo) throws SQLException  {
        
        conexao.setDeclaracao("SELECT * FROM TipoLogradouro WHERE  idTipoLogradouro = ?");
        conexao.getDeclaracao().setInt(1, tipo.getIdTipoLogradouro());
      
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        resultado.last();
        
        //Seta o ID do objeto tipologradouro para a ID recebida pelo query
        // Observe que se usa o resultado).getInt("NOME DO ELEMENTO DA TABELA")
        
        return PopulaTipoLogradouro(resultado);
    }
    
    public Boolean Update(TipoLogradouro tipoLogradouro) throws SQLException{
        
        conexao.setDeclaracao("UPDATE nomeTipoLogradouro,siglaTipoLogradouro FROM TipoLogradouro SET (?,?) WHERE idTipoLogradouro = ?");
        
        conexao.getDeclaracao().setString(1, tipoLogradouro.getNomeTipoLogradouro());
        conexao.getDeclaracao().setString(2, tipoLogradouro.getSiglaTipoLogradouro());
        conexao.getDeclaracao().setInt(3, tipoLogradouro.getIdTipoLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(TipoLogradouro tipoLogradouro) throws SQLException{
        
        conexao.setDeclaracao("DELETE FROM TipoLogradouro WHERE idTipoLogradouro = ?");
        
        conexao.getDeclaracao().setInt(1, tipoLogradouro.getIdTipoLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<TipoLogradouro> List() throws SQLException{
        ArrayList<TipoLogradouro> tiposLogradouro = new ArrayList<>();
        
        conexao.setDeclaracao("SELECT * FROM TipoLogradouro");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            TipoLogradouro tipoLogradouro = new TipoLogradouro();
            tipoLogradouro = PopulaTipoLogradouro(resultado);
            tiposLogradouro.add(tipoLogradouro);
        }
        
        return tiposLogradouro;
    }

    public TipoLogradouro Read(int idTipoLogradouro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
