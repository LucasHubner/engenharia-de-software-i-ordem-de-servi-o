/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.Cep;
import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.col.endereco.ColEndereco;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.manager.endereco.*;

/**
 *
 * @author lucas
 */
public class ManagerEndereco {
 Conexao conexao;
 ManagerBairro mBairro = new ManagerBairro();
 ManagerCidade mCidade = new ManagerCidade();
 ManagerLogradouro mLogradouro = new ManagerLogradouro();
 ManagerPais mPais = new ManagerPais();
 
     public void validar(Endereco endereco) throws EnderecoException, SQLException{
        if(endereco.getBairro() == null){
            throw new EnderecoException("" + "Atributo 'Bairro' está vazio. \n"
                                           + "Endereço não pode ser criado");
        }
        else if(mBairro.recuperaBairro(endereco.getBairro().getIdBairro()) == null){
            throw new EnderecoException("Bairro não existe");
        }
        else if((mBairro.recuperaBairro(endereco.getBairro().getIdBairro())).isEmpty()){
            throw new EnderecoException("" + "Bairro não encontrado");
        }
        
        if(endereco.getCep()== null || endereco.getCep().getCepString().isEmpty()){
            throw new EnderecoException("" + "Atributo 'Cep' está vazio. \n"
                                           + "Endereço não pode ser criado");
        }
        if(endereco.getCidade()== null){
            throw new EnderecoException("" + "Atributo 'Cidade' está vazio. \n"
                                           + "Endereço não pode ser criado");
        }
        else if((mCidade.recuperaCidade(endereco.getCidade().getIdCidade())).isEmpty()){
            throw new EnderecoException("" + "Cidade não encontrada");
        }

        
        if(endereco.getLogradouro()== null){
            throw new EnderecoException("" + "Atributo 'Logradouro' está vazio. \n"
                                           + "Endereço não pode ser criado");
        }
        else if ((mLogradouro.recuperaLogradouro(endereco.getLogradouro().getIdLogradouro()).isEmpty())){
            throw new EnderecoException("" + "Logradouro não encontrado");
        }
        
        if(endereco.getPais()== null){
            throw new EnderecoException("" + "Atributo 'Pais' está vazio. \n"
                                           + "Endereço não pode ser criado");
        }
        else if((mPais.recuperaPais(endereco.getPais().getIdPais()).isEmpty())) {
            throw new EnderecoException("" + "Pais não encontrado");
        }
    }
 
    public Endereco CadastraEndereco(Endereco endereco) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(endereco);
        conexao.AbreConexao();
        ColEndereco col = new ColEndereco(conexao);
        endereco = col.Create(endereco);
        conexao.FechaConexao();
        return endereco;
    }
    
    public Endereco excluiEndereco(Endereco endereco) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEndereco col = new ColEndereco(conexao);
        col.Remove(endereco);
        conexao.FechaConexao();
        return endereco;
    }
    
    public ArrayList<Endereco> listaEnderecos() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEndereco col = new ColEndereco(conexao);
        ArrayList<Endereco> enderecos = col.List();
        conexao.FechaConexao();
        return enderecos;
    }
    
    public Endereco recuperaEndereco(int idEndereco) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEndereco col = new ColEndereco(conexao);
        Endereco bairro = col.Read(idEndereco);
        conexao.FechaConexao();
        return bairro;
    }
}
