/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.col.endereco.ColUnidadeFederativa;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerUnidadeFederativa {
     Conexao conexao;
     
    public void validar(UnidadeFederativa uf) throws EnderecoException{
        
        if(uf.getNomeUnidadeFederativa()== null || uf.getNomeUnidadeFederativa().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
     
    public UnidadeFederativa CadastraUnidadeFederativa(UnidadeFederativa uf) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(uf);
        conexao.AbreConexao();
        ColUnidadeFederativa col = new ColUnidadeFederativa(conexao);
        uf = col.Create(uf);
        conexao.FechaConexao();
        return uf;
    }
    
    public UnidadeFederativa excluiUnidadeFederativa(UnidadeFederativa uf) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColUnidadeFederativa col = new ColUnidadeFederativa(conexao);
        col.Remove(uf);
        conexao.FechaConexao();
        return uf;
    }
    
    public ArrayList<UnidadeFederativa> listaUnidadeFederativas() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColUnidadeFederativa col = new ColUnidadeFederativa(conexao);
        ArrayList<UnidadeFederativa> ufs = col.List();
        conexao.FechaConexao();
        return ufs;
    }
    
    public UnidadeFederativa recuperaUnidadeFederativa(int idUnidadeFederativa) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColUnidadeFederativa col = new ColUnidadeFederativa(conexao);
        UnidadeFederativa bairro = col.Read(idUnidadeFederativa);
        conexao.FechaConexao();
        return bairro;
    }
}
