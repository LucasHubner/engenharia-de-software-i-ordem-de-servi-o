/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.TipoLogradouro;
import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.col.endereco.ColTipoLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerTipoLogradouro {
     Conexao conexao;
     
    public void validar(TipoLogradouro tipo) throws EnderecoException{
        if(tipo.getNomeTipoLogradouro()== null || tipo.getNomeTipoLogradouro().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public TipoLogradouro CadastraTipoLogradouro(TipoLogradouro tipoLogradouro) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(tipoLogradouro);
        conexao.AbreConexao();
        ColTipoLogradouro col = new ColTipoLogradouro(conexao);
        tipoLogradouro = col.Create(tipoLogradouro);
        conexao.FechaConexao();
        return tipoLogradouro;
    }
    public TipoLogradouro RecuperaTipoLogradouro(TipoLogradouro tipoLogradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoLogradouro col = new ColTipoLogradouro(conexao);
        tipoLogradouro = col.Read(tipoLogradouro);
        conexao.FechaConexao();
        return tipoLogradouro;
    }
    
    public TipoLogradouro excluiTipoLogradouro(TipoLogradouro tipoLogradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoLogradouro col = new ColTipoLogradouro(conexao);
        col.Remove(tipoLogradouro);
        conexao.FechaConexao();
        return tipoLogradouro;
    }
    
    public ArrayList<TipoLogradouro> listaTipoLogradouros() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoLogradouro col = new ColTipoLogradouro(conexao);
        ArrayList<TipoLogradouro> tipoLogradouros = col.List();
        conexao.FechaConexao();
        return tipoLogradouros;
    }
    
    public TipoLogradouro recuperaTipoLogradouro(int idTipoLogradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoLogradouro col = new ColTipoLogradouro(conexao);
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        tipoLogradouro.setIdTipoLogradouro(idTipoLogradouro);
        tipoLogradouro = col.Read(tipoLogradouro);
        conexao.FechaConexao();
        return tipoLogradouro;
    }
    
}
