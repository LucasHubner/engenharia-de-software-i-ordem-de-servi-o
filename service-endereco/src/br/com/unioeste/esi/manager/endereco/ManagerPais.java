/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.Pais;
import br.com.unioeste.esi.bo.endereco.Endereco;
import br.com.unioeste.esi.col.endereco.ColPais;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerPais {
 Conexao conexao;
 
    public void validar(Pais pais) throws EnderecoException{
        if(pais.getNomePais()== null || pais.getNomePais().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Pais CadastraPais(Pais pais) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(pais);
        conexao.AbreConexao();
        ColPais col = new ColPais(conexao);
        pais = col.Create(pais);
        conexao.FechaConexao();
        return pais;
    }
    
    public Pais excluiPais(Pais pais) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColPais col = new ColPais(conexao);
        col.Remove(pais);
        conexao.FechaConexao();
        return pais;
    }
    
    public ArrayList<Pais> listaPaises() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColPais col = new ColPais(conexao);
        ArrayList<Pais> paises = col.List();
        conexao.FechaConexao();
        return paises;
    }
    
        public Pais recuperaPais(int idPais) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColPais col = new ColPais(conexao);
        Pais bairro = col.Read(idPais);
        conexao.FechaConexao();
        return bairro;
    }
}
