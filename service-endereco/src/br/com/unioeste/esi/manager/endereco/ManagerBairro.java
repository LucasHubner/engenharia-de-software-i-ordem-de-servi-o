/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.Bairro;
import br.com.unioeste.esi.col.endereco.ColBairro;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.exception.endereco.EnderecoException;

/**
 *
 * @author lucas
 */
public class ManagerBairro {
    Conexao conexao;
    
    public void validar(Bairro bairro) throws EnderecoException{
        if(bairro.getNomeBairro() == null || bairro.getNomeBairro().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }

    public Bairro CadastraBairro(Bairro bairro) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(bairro);
        conexao.AbreConexao();
        ColBairro col = new ColBairro(conexao);
        bairro = col.Create(bairro);
        conexao.FechaConexao();
        return bairro;
    }
    
    public Bairro excluiBairro(Bairro bairro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColBairro col = new ColBairro(conexao);
        col.Remove(bairro);
        conexao.FechaConexao();
        return bairro;
    }
    
    public ArrayList<Bairro> listaBairros() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColBairro col = new ColBairro(conexao);
        ArrayList<Bairro> bairros = col.List();
        conexao.FechaConexao();
        return bairros;
    }
    
    public Bairro recuperaBairro(int idBairro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColBairro col = new ColBairro(conexao);
        Bairro bairro = col.Read(idBairro);
        conexao.FechaConexao();
        return bairro;
    }
    
}
