/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.Logradouro;
import br.com.unioeste.esi.col.endereco.ColLogradouro;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerLogradouro {
     Conexao conexao;
     ManagerTipoLogradouro mTipo = new ManagerTipoLogradouro();
     
    public void validar(Logradouro logradouro) throws EnderecoException, SQLException{
        if(mTipo.recuperaTipoLogradouro(logradouro.getTipoLogradouro().getIdTipoLogradouro()) == null){
            throw new EnderecoException("Tipo Logradouro não existente");
        }
        
        if(logradouro.getNomeLogradouro() == null || logradouro.getNomeLogradouro().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public Logradouro CadastraLogradouro(Logradouro logradouro) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(logradouro);
        conexao.AbreConexao();
        ColLogradouro col = new ColLogradouro(conexao);
        logradouro = col.Create(logradouro);
        conexao.FechaConexao();
        return logradouro;
    }
    
    public Logradouro excluiLogradouro(Logradouro logradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColLogradouro col = new ColLogradouro(conexao);
        col.Remove(logradouro);
        conexao.FechaConexao();
        return logradouro;
    }
    
    public ArrayList<Logradouro> listaLogradouros(int idTipoLogradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColLogradouro col = new ColLogradouro(conexao);
        ArrayList<Logradouro> logradouros = col.ListByTipoLogradouro(idTipoLogradouro);
        conexao.FechaConexao();
        return logradouros;
    }
    
        public Logradouro recuperaLogradouro(int idLogradouro) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColLogradouro col = new ColLogradouro(conexao);
        Logradouro bairro = col.Read(idLogradouro);
        conexao.FechaConexao();
        return bairro;
    }
}
