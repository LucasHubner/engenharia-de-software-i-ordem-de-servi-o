/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.endereco;

import br.com.unioeste.esi.bo.endereco.Cidade;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import br.com.unioeste.esi.col.endereco.ColCidade;
import br.com.unioeste.esi.exception.endereco.EnderecoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerCidade {
    Conexao conexao;
    ManagerUnidadeFederativa mUF = new ManagerUnidadeFederativa();
    
    public void validar(Cidade cidade) throws EnderecoException, SQLException{

        if(mUF.recuperaUnidadeFederativa(cidade.getUnidadeFederativa().getIdUnidadeFederativa()) == null){
            throw new EnderecoException("Unidade Federativa não existente");
        }
        
        if(cidade.getNomeCidade()== null || cidade.getNomeCidade().equals("")){
            throw new EnderecoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
        
    }
    
    public Cidade CadastraCidade(Cidade cidade) throws SQLException, EnderecoException{
        conexao = new Conexao();
        validar(cidade);
        conexao.AbreConexao();
        ColCidade col = new ColCidade(conexao);
        cidade = col.Create(cidade);
        conexao.FechaConexao();
        return cidade;
    }
    
    public Cidade excluiCidade(Cidade cidade) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCidade col = new ColCidade(conexao);
        col.Remove(cidade);
        conexao.FechaConexao();
        return cidade;
    }
    
    public ArrayList<Cidade> listaCidadesPorUf(UnidadeFederativa uf) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCidade col = new ColCidade(conexao);
        
        ArrayList<Cidade> cidades = col.ListByUnidadeFederativa(uf);
        conexao.FechaConexao();
        return cidades;
    }
    
    public Cidade recuperaCidade(int idCidade) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColCidade col = new ColCidade(conexao);
        Cidade cidade = col.Read(idCidade);
        conexao.FechaConexao();
        return cidade;
    }
    
}
