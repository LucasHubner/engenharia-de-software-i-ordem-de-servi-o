/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa;

import java.io.Serializable;

/**
 *
 * @author Pedro
 */
public class NomePessoaJuridica implements Serializable{
    String nomeFantasia;
    String nomeEmpresa;

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public NomePessoaJuridica() {
    }

    public NomePessoaJuridica(String nomeFantasia, String nomeEmpresa) {
        this.nomeFantasia = nomeFantasia;
        this.nomeEmpresa = nomeEmpresa;
    }
    
    
}
