/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.fisica;
import br.com.unioeste.esi.bo.endereco.UnidadeFederativa;
import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class OrgaoExpeditor implements Serializable {
    int idOrgaoExpeditor;
    String nomeCompleto;
    String nomeAbreviado;
     UnidadeFederativa uf;

    public int getIdOrgaoExpeditor() {
        return idOrgaoExpeditor;
    }

    public void setIdOrgaoExpeditor(int idOrgaoExpeditor) {
        this.idOrgaoExpeditor = idOrgaoExpeditor;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    public UnidadeFederativa getUf() {
        return uf;
    }

    public void setUf(UnidadeFederativa uf) {
        this.uf = uf;
    }
     
     
}
