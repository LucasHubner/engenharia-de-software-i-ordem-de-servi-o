/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.fisica;

import br.com.unioeste.esi.bo.pessoa.Pessoa;
import br.com.unioeste.esi.bo.pessoa.fisica.RG;
import br.com.unioeste.esi.bo.pessoa.fisica.CPF;
import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class PessoaFisica extends Pessoa implements Serializable{
    CPF cpf;
    RG rg;
    Genero sexo;

    public RG getRg() {
        return rg;
    }

    public void setRg(RG rg) {
        this.rg = rg;
    }

    public CPF getCpf() {
        return cpf;
    }

    public void setCpf(CPF cpf) {
        this.cpf = cpf;
    }

    public Genero getSexo() {
        return sexo;
    }

    public void setGenero(Genero sexo) {
        this.sexo = sexo;
    }
    
}
