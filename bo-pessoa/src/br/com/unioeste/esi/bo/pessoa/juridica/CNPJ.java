/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.juridica;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author lucas
 */
public class CNPJ implements Serializable {
    int numero;
    String numeroEstendido;
    Date dataEmissao;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNumeroEstendido() {
        return numeroEstendido;
    }

    public void setNumeroEstendido(String numeroEstendido) {
        this.numeroEstendido = numeroEstendido;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }
    
    
    
}
