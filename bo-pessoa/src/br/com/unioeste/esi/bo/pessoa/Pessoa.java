/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa;

import br.com.unioeste.esi.bo.endereco.EnderecoEspecifico;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public abstract class Pessoa{
    NomePessoaFisica nome;
    NomePessoaJuridica nomeJ;
    EnderecoEspecifico enderecoPrincipal;
    ArrayList<Telefone> telefones;

    public ArrayList<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<Telefone> telefones) {
        this.telefones = telefones;
    }
    
    public NomePessoaFisica getNome() {
        return nome;
    }

    public NomePessoaJuridica getNomeJ() {
        return nomeJ;
    }

    public void setNomeJ(NomePessoaJuridica nomeJ) {
        this.nomeJ = nomeJ;
    }

    public void setNome(NomePessoaFisica nome) {
        this.nome = nome;
    }

    public EnderecoEspecifico getEnderecoPrincipal() {
        return enderecoPrincipal;
    }

    public void setEnderecoPrincipal(EnderecoEspecifico enderecoPrincipal) {
        this.enderecoPrincipal = enderecoPrincipal;
    }
    
    
}
