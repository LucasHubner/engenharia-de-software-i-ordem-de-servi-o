/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa;

import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class TipoEnderecoVirtual implements Serializable {
    int idTipoEnderecoVirtual;
    String nomeTipoEnderecoVirtual;

    public int getIdTipoEnderecoVirtual() {
        return idTipoEnderecoVirtual;
    }

    public void setIdTipoEnderecoVirtual(int idTipoEnderecoVirtual) {
        this.idTipoEnderecoVirtual = idTipoEnderecoVirtual;
    }

    public String getNomeTipoEnderecoVirtual() {
        return nomeTipoEnderecoVirtual;
    }

    public void setNomeTipoEnderecoVirtual(String nomeTipoEnderecoVirtual) {
        this.nomeTipoEnderecoVirtual = nomeTipoEnderecoVirtual;
    }
    
    
}
