/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa;

import br.com.unioeste.esi.bo.pessoa.juridica.PessoaJuridica;
import br.com.unioeste.esi.bo.pessoa.fisica.PessoaFisica;

/**
 *
 * @author lucas
 */
public class Cliente {
    int idCliente;
    PessoaFisica pessoaFisica;
    PessoaJuridica pessoaJuridica;

    public Cliente() {
        this.pessoaFisica = new PessoaFisica();
        this.pessoaJuridica = new PessoaJuridica();
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }
    
    
    
}
