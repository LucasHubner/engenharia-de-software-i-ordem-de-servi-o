/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.fisica;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author lucas
 */
public class RG implements Serializable {
    String Numero;
    Date dataEmissão;
    OrgaoExpeditor orgaoExpeditor;

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public Date getDataEmissão() {
        return dataEmissão;
    }

    public void setDataEmissão(Date dataEmissão) {
        this.dataEmissão = dataEmissão;
    }

    public OrgaoExpeditor getOrgaoExpeditor() {
        return orgaoExpeditor;
    }

    public void setOrgaoExpeditor(OrgaoExpeditor orgaoExpeditor) {
        this.orgaoExpeditor = orgaoExpeditor;
    }
    
    
    
}
