/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.juridica;

import br.com.unioeste.esi.bo.pessoa.Pessoa;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class PessoaJuridica extends Pessoa implements Serializable {
    CNPJ cnpj;
    ArrayList<AtividadeComercial> atividadesComerciais;

    public CNPJ getCnpj() {
        return cnpj;
    }

    public void setCnpj(CNPJ cnpj) {
        this.cnpj = cnpj;
    }

    public ArrayList<AtividadeComercial> getAtividadesComerciais() {
        return atividadesComerciais;
    }

    public void setAtividadesComerciais(ArrayList<AtividadeComercial> atividadesComerciais) {
        this.atividadesComerciais = atividadesComerciais;
    }
    
}
