/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.bo.pessoa.fisica;

import java.io.Serializable;

/**
 *
 * @author lucas
 */
public class CPF implements Serializable {
    String cpfString = new String();
    int cpfInt;
    String cpfFormatado;

    public String getCpfString() {
        return cpfString;
    }

    public void setCpfString(String cpfString) {
        this.cpfString = cpfString;
    }

    public int getCpfInt() {
        return cpfInt;
    }

    public void setCpfInt(int cpfInt) {
        this.cpfInt = cpfInt;
    }

    public String getCpfFormatado() {
        return cpfFormatado;
    }

    public void setCpfFormatado(String cpfFormatado) {
        this.cpfFormatado = cpfFormatado;
    }
    
    public boolean isEmpty(){
        if (this.cpfString.equals("")){
            return true;
        }
        return false;
    }
}
