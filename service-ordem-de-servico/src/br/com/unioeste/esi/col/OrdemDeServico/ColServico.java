/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.Servico;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerTipoServico;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColServico {
    Conexao conexao;

    public ColServico(Conexao conexao) {
        this.conexao = conexao;
    }
    private Servico PopulaServico(ResultSet resultado) throws SQLException{
        Servico servico = new Servico();
        servico.setNroOrdemDeServico(resultado.getInt("nroOrdemDeServico"));
        servico.setTempo(resultado.getFloat("tempo"));
        servico.setTipoServico(new ManagerTipoServico().recuperaEstadoOS(resultado.getInt("idTipoServico")));
        servico.setValor(resultado.getFloat("valor"));
        return servico;
        
    }
    public Servico Create(Servico servico) throws SQLException{
        conexao.setDeclaracao("INSERT INTO Servico "
                            + "(nroOrdemDeServico, " // 1
                            + "idTipoServico, "      // 2
                            + "tempo, "              // 3
                            + "valor) "              // 4
                + " VALUES "
                + "(?,?,?,?");
        conexao.getDeclaracao().setInt(1, servico.getNroOrdemDeServico());
        conexao.getDeclaracao().setInt(2, servico.getTipoServico().getId());
        conexao.getDeclaracao().setFloat(3, servico.getTempo());
        conexao.getDeclaracao().setFloat(4, servico.getNroOrdemDeServico());

        conexao.getDeclaracao().executeUpdate();
        
        return servico;
    }
    
    public Servico Read(Servico servico) throws SQLException{
        conexao.setDeclaracao("SELECT * FROM Servico WHERE nroOrdemDeServico = ? AND idTipoServico = ?");
        conexao.getDeclaracao().setInt(1, servico.getNroOrdemDeServico());
        conexao.getDeclaracao().setInt(2,servico.getTipoServico().getId());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        return PopulaServico(resultado);
    }
    
    public ArrayList<Servico> GetServicosFromOS(int nroOrdemDeServico) throws SQLException{
        ArrayList<Servico> servicos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Servico WHERE nroOrdemDeServico = ?");
        conexao.getDeclaracao().setInt(1, nroOrdemDeServico);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        while(resultado.next()){
            servicos.add(PopulaServico(resultado));
        }
        
        return servicos;
    }
     public Servico Delete(Servico servico) throws SQLException{
        conexao.setDeclaracao("DELETE FROM Servico WHERE nroOrdemDeServico = ? ");
        conexao.getDeclaracao().setInt(1, servico.getNroOrdemDeServico());
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        return PopulaServico(resultado);
    }
}
