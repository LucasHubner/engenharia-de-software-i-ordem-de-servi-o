/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.OrdemDeServico;
import br.com.unioeste.esi.infra.bd.Conexao;
import br.com.unioeste.esi.manager.OrdemDeServico.ManagerEstadoOS;
import br.com.unioeste.esi.manager.pessoa.ManagerCliente;
import br.com.unioeste.esi.manager.veiculo.ManagerVeiculo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author lucas
 */
public class ColOrdemDeServico {
    
      Conexao conexao;

    public ColOrdemDeServico(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private OrdemDeServico PopulaOrdemDeServico(ResultSet resultado) throws SQLException{
        OrdemDeServico ordem = new OrdemDeServico();
        ordem.setNumero(resultado.getInt("nroOrdemDeServico"));
        
        ordem.setVeiculo(new ManagerVeiculo().recuperaVeiculo(resultado.getString("placaCarro")));
        ordem.setCliente(new ManagerCliente().recuperaCliente(resultado.getInt("idCliente")));
        ordem.setEstado(new ManagerEstadoOS().recuperaEstadoOS(resultado.getInt("idSituacao")));
        ordem.setDescricao(resultado.getString("Descricao"));
        ordem.setDataAbertura(resultado.getDate("dataInicio"));
        ordem.setDataFechamento(resultado.getDate("dataFim"));
        
        
        return ordem;
    }
    
    public OrdemDeServico Create(OrdemDeServico ordem) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `OrdemDeServico` "
                                    + "(nroOrdemDeServico, " // 1
                                    + "placaCarro, "         // 2
                                    + "idCliente, "          // 3
                                    + "idSituacao, "         // 4
                                    + "Descricao, "          // 5
                                    + "dataInicio, "         // 6
                                    + "dataFim"              // 7
                + ") VALUES (null,?,?,?,?,?,?)");

        conexao.getDeclaracao().setString(1, ordem.getVeiculo().getPlaca());
        conexao.getDeclaracao().setInt(2, ordem.getCliente().getIdCliente());
        conexao.getDeclaracao().setInt(3, ordem.getEstado().getId());
        conexao.getDeclaracao().setString(4, ordem.getDescricao());
        conexao.getDeclaracao().setDate(5, new java.sql.Date(ordem.getDataAbertura().getTime()));
        conexao.getDeclaracao().setDate(6, new java.sql.Date(ordem.getDataFechamento().getTime()));
        
        conexao.getDeclaracao().executeUpdate();

        return ordem;
    }
    
    public OrdemDeServico Read (OrdemDeServico ordem) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM OrdemDeServico WHERE nroOrdemDeServico = ?");
        
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaOrdemDeServico(resultado);
    }
    
    public Boolean Update (OrdemDeServico ordem) throws SQLException {
        
        conexao.setDeclaracao("UPDATE "
                                    + "placaCarro, "         // 1
                                    + "idCliente, "          // 2
                                    + "idSituacao, "         // 3
                                    + "Descricao, "          // 4
                                    + "dataInicio, "         // 5
                                    + "dataFim"              // 6
                            + ")  FROM OrdemDeServico SET (?,?,?,?,?,?,?)"
                + "WHERE nroOrdemDeServico = ?");            // 7

                
        conexao.getDeclaracao().setString(1, ordem.getVeiculo().getPlaca());
        conexao.getDeclaracao().setInt(2, ordem.getCliente().getIdCliente());
        conexao.getDeclaracao().setInt(3, ordem.getEstado().getId());
        conexao.getDeclaracao().setString(4, ordem.getDescricao());
        conexao.getDeclaracao().setDate(5, new java.sql.Date(ordem.getDataAbertura().getTime()));
        conexao.getDeclaracao().setDate(6, new java.sql.Date(ordem.getDataFechamento().getTime()));
        conexao.getDeclaracao().setInt(7, ordem.getNumero());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (OrdemDeServico ordem) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM OrdemDeServico WHERE nroOrdemDeServico = ?");
        
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<OrdemDeServico> List() throws SQLException{
        ArrayList<OrdemDeServico> ordemes = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM OrdemDeServico");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            OrdemDeServico ordem = PopulaOrdemDeServico(resultado);
            ordemes.add(ordem);
        }
        
        return ordemes;
    }
}
