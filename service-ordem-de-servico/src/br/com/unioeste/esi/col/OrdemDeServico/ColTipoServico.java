/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.TipoServico;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColTipoServico {
    Conexao conexao;
   
    public ColTipoServico(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private TipoServico PopulaTipoServico(ResultSet resultado) throws SQLException{
        TipoServico tipo = new TipoServico();
        
        tipo.setId(resultado.getInt("idTipoServico"));
        tipo.setNome(resultado.getString("nome"));
        
        return tipo;
    }
    
    public TipoServico Create(TipoServico tipo) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `TipoServico` (`idTipoServico`, `nome`) VALUES (NULL, '"+tipo.getNome()+"')");
        conexao.getDeclaracao().executeUpdate();
        tipo.setId(conexao.getLastInsertedId());
        
        return tipo;
    }
    
    public TipoServico Read (int idTipoServico) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM TipoServico WHERE idTipoServico = ?");
        
        conexao.getDeclaracao().setInt(1, idTipoServico);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaTipoServico(resultado);
    }
    
    public Boolean Update (TipoServico tipo) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome FROM TipoServico SET (?) WHERE idTipoServico = ?");
        
        conexao.getDeclaracao().setString(1, tipo.getNome());
        conexao.getDeclaracao().setInt(2, tipo.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (TipoServico tipo) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM TipoServico WHERE idTipoServico = ?");
        
        conexao.getDeclaracao().setInt(1, tipo.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<TipoServico> List() throws SQLException{
        ArrayList<TipoServico> tipos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM TipoServico");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            TipoServico tipo = PopulaTipoServico(resultado);
            tipos.add(tipo);
        }
        
        return tipos;
    }
}
