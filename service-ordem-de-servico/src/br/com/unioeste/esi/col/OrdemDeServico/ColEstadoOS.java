/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.col.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.EstadoOS;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ColEstadoOS {
    Conexao conexao;
   
    public ColEstadoOS(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private EstadoOS PopulaEstadoOS(ResultSet resultado) throws SQLException{
        EstadoOS estado = new EstadoOS();
        
        estado.setId(resultado.getInt("idEstado"));
        estado.setNome(resultado.getString("nome"));
        
        return estado;
    }
    
    public EstadoOS Create(EstadoOS estado) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `EstadoOrdemDeServico` (`idEstado`, `nome`) VALUES (NULL, '"+estado.getNome()+"')");
        conexao.getDeclaracao().executeUpdate();
        estado.setId(conexao.getLastInsertedId());
        
        return estado;
    }
    
    public EstadoOS Read (int idEstadoOS) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM EstadoOrdemDeServico WHERE idEstado = ?");
        
        conexao.getDeclaracao().setInt(1, idEstadoOS);
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        resultado.last();
        
        return PopulaEstadoOS(resultado);
    }
    
    public Boolean Update (EstadoOS estado) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nome FROM EstadoOrdemDeServico SET (?) WHERE idEstado = ?");
        
        conexao.getDeclaracao().setString(1, estado.getNome());
        conexao.getDeclaracao().setInt(2, estado.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (EstadoOS estado) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM EstadoOrdemDeServico WHERE idEstado = ?");
        
        conexao.getDeclaracao().setInt(1, estado.getId());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<EstadoOS> List() throws SQLException{
        ArrayList<EstadoOS> estados = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM EstadoOrdemDeServico");
        ResultSet resultado = conexao.getDeclaracao().executeQuery();
        
        while(resultado.next()){
            EstadoOS estado = PopulaEstadoOS(resultado);
            estados.add(estado);
        }
        
        return estados;
    }
}
