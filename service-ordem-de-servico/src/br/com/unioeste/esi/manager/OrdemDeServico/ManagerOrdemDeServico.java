/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.OrdemDeServico;
import br.com.unioeste.esi.col.OrdemDeServico.ColOrdemDeServico;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.exception.OrdemDeServico.OrdemDeServicoException;

/**
 *
 * @author lucas
 */
public class ManagerOrdemDeServico {
    Conexao conexao;
    OrdemDeServico ordemDeServico;
    
    public void validar() throws OrdemDeServicoException{
        
    }
    
    public OrdemDeServico CadastraOrdemDeServico(OrdemDeServico estado) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrdemDeServico col = new ColOrdemDeServico(conexao);
        estado = col.Create(estado);
        conexao.FechaConexao();
        return estado;
    }
    
    public OrdemDeServico excluiOrdemDeServico(OrdemDeServico estado) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrdemDeServico col = new ColOrdemDeServico(conexao);
        col.Remove(estado);
        conexao.FechaConexao();
        return estado;
    }
    
    public ArrayList<OrdemDeServico> listaOrdemDeServicos() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrdemDeServico col = new ColOrdemDeServico(conexao);
        ArrayList<OrdemDeServico> estados = col.List();
        conexao.FechaConexao();
        return estados;
    }
    
    public OrdemDeServico recuperaOrdemDeServico(OrdemDeServico os) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColOrdemDeServico col = new ColOrdemDeServico(conexao);
        OrdemDeServico ordemDeServico = col.Read(os);
        conexao.FechaConexao();
        return ordemDeServico;
    }
    
}
