/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.EstadoOS;
import br.com.unioeste.esi.col.OrdemDeServico.ColEstadoOS;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.unioeste.esi.exception.OrdemDeServico.OrdemDeServicoException;

/**
 *
 * @author lucas
 */
public class ManagerEstadoOS {
    Conexao conexao;
    EstadoOS estadoOS;
    
    public void validar() throws OrdemDeServicoException{
        if(estadoOS.getNome()== null || estadoOS.getNome().equals("")){
            throw new OrdemDeServicoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public EstadoOS CadastraEstadoOS(EstadoOS estado) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEstadoOS col = new ColEstadoOS(conexao);
        estado = col.Create(estado);
        conexao.FechaConexao();
        return estado;
    }
    
    public EstadoOS excluiEstadoOS(EstadoOS estado) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEstadoOS col = new ColEstadoOS(conexao);
        col.Remove(estado);
        conexao.FechaConexao();
        return estado;
    }
    
    public ArrayList<EstadoOS> listaEstadoOSs() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEstadoOS col = new ColEstadoOS(conexao);
        ArrayList<EstadoOS> estados = col.List();
        conexao.FechaConexao();
        return estados;
    }
    
    public EstadoOS recuperaEstadoOS(int idEstadoOS) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColEstadoOS col = new ColEstadoOS(conexao);
        EstadoOS estadoOS = col.Read(idEstadoOS);
        conexao.FechaConexao();
        return estadoOS;
    }
    
}
