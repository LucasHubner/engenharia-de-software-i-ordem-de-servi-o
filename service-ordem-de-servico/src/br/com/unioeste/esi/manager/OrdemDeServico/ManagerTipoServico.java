/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unioeste.esi.manager.OrdemDeServico;

import br.com.unioeste.esi.bo.OrdemDeServico.TipoServico;
import br.com.unioeste.esi.col.OrdemDeServico.ColTipoServico;
import br.com.unioeste.esi.exception.OrdemDeServico.OrdemDeServicoException;
import br.com.unioeste.esi.infra.bd.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class ManagerTipoServico {
    Conexao conexao;
    TipoServico tipoServico;
    
    public void validar() throws OrdemDeServicoException{
        if(tipoServico.getNome()== null || tipoServico.getNome().equals("")){
            throw new OrdemDeServicoException("" + "Atributo 'Nome' está vazio. \n"
                                           + "Este atributo é de preenchimento obrigatório");
        }
    }
    
    public TipoServico CadastraTipoServico(TipoServico tipo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoServico col = new ColTipoServico(conexao);
        tipo = col.Create(tipo);
        conexao.FechaConexao();
        return tipo;
    }
    
    public TipoServico excluiTipoServico(TipoServico tipo) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoServico col = new ColTipoServico(conexao);
        col.Remove(tipo);
        conexao.FechaConexao();
        return tipo;
    }
    
    public ArrayList<TipoServico> listaTipoServicos() throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoServico col = new ColTipoServico(conexao);
        ArrayList<TipoServico> tipos = col.List();
        conexao.FechaConexao();
        return tipos;
    }
    
    public TipoServico recuperaEstadoOS(int idTipoServico) throws SQLException{
        conexao = new Conexao();
        conexao.AbreConexao();
        ColTipoServico col = new ColTipoServico(conexao);
        TipoServico tipoServico = col.Read(idTipoServico);
        conexao.FechaConexao();
        return tipoServico;
    }
    
}
